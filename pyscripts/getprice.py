
# from twisted.internet import reactor
# from scrapy.crawler import Crawler
# from scrapy.settings import Settings
# from scrapy import log
# from testspiders.spiders.followall import FollowAllSpider
# import scrapy

import requests
from bs4 import BeautifulSoup
import os
import argparse
import numpy as np
import re
from fuzzywuzzy import fuzz

# class ItPriceSpider(scrapy.Spider):
#   name = "itprice"
#   allowed_domains = ["itprice.com"]
#   start_urls = [
#       "http://www.itprice.com/Computers/Programming/Languages/Python/Books/"
#   ]

#   def __init__(self, brand, model) -> None:
#     self.start_urls = ["http://www.itprice.com/" + brand.lower() + "-price-list/" + model.replace(' ','%20')+'.html']

#   def parse(self, response):
#     filename = response.url.split("/")[-2]

# def get_price(brand, model):
#   spider = ItPriceSpider(brand, model)
#   crawler = Crawler(Settings())
#   crawler.configure()
#   crawler.crawl(spider)
#   crawler.start()
#   log.start()
#   reactor.run() # the script will block here


def get_price_itprice(brand, model):
  if brand.lower() == 'apple':
    return None
  filename = model.replace(' ','%20') + '.html'
  cachedpath = "cache/itprice/" + brand 
  try:
    with open(cachedpath + '/' + filename) as f:
      print('used cached version')
      source = f.read()
  except:
    url = "https://itprice.com/" + brand.lower() + "-price-list/" + filename
    print('download',url)
    try:
      source = requests.get(url).text
      os.makedirs(cachedpath, exist_ok=True)
      with open(cachedpath + '/' + filename, "w") as f:
        f.write(source)
    except Exception as e:
      print('failed to download', e)
      return None

  # print(source)

  soup = BeautifulSoup(source, 'html5lib')
  table = soup.find('table',id='choice_product')
  # print(table)
  if table:
    price_list = []
    for row in table.find_all('tr'):
      cols = row.find_all('td')
      # print(cols)
      if len(cols) == 7:
        price_str = cols[3].text.strip().replace('$','').replace(',','')
        print("convert",price_str)
        price = float(price_str)
        price_list.append(price)
    if len(price_list)>0:
      return int(np.median(np.array(price_list)))
    else:
      return None
  else:
    return None

def get_price_shi(brand, model):
  # https://www.shi.com/shop/search/hardware/computers-and-tablets?k=chromebook+enterprise+x360+14e+g1&sk=1
  if brand.lower() == 'apple':
    return None
  filename = model.replace(' ','_') + '.html'
  cachedpath = "cache/shi/" + brand 
  url = "https://www.shi.com/shop/search/hardware/computers-and-tablets?k=" + brand.lower() + "+" + model.replace(' ','+')
  try:
    with open(cachedpath + '/' + filename) as f:
      print('used cached version', (cachedpath + '/' + filename), 'of', url)
      source = f.read()
  except:
    
    print('download',url)
    try:
      source = requests.get(url).text
      os.makedirs(cachedpath, exist_ok=True)
      with open(cachedpath + '/' + filename, "w") as f:
        f.write(source)
    except Exception as e:
      print('failed to download', e)
      return None
  soup = BeautifulSoup(source, 'html5lib')
  if soup.find('div', class_='noResults-SR'):
    print('no results 1')
    return None
  #results = soup.find('div',id='srResultsDiv')
  results = soup.find('div',class_='srResults')
  if not results:
    print('no results 2')
    return None
  products = results.find_all('div', class_='srProductDetails')
  price_list = []
  for p in products:
    a = p.find('a', class_='srh_pr.pnm')
    if fuzz.ratio(model,a.text)>50:
      i = p.find('price-range')
      price_str = i.text.strip().replace('&nbsp;','').replace(',','').replace('$','')
      if price_str!='Login for Price':
        print("convert",price_str)
        price = float(price_str)
        price_list.append(price)
    else:
      print('no match', model, a.text, fuzz.ratio(model,a.text))
  if len(price_list)>0:
    return int(np.median(np.array(price_list)))
  else:
    return None

# https://admistore.fr/recherche?search_query=dell+Precision+Tower+3630#elasticsearch_category_18=18
def get_price_admistore(brand, model, category):
  # if brand.lower() == 'apple':
    # return None
  model0 = model
  model = model.replace('Micro Form Factor','')
  model = model.replace('Small Form Factor','')
  model = model.replace('16-inch','16.2"')
  model = model.replace('13-inch','13.3"')
  model = model.replace('-inch','"')
  model = model.strip()
  filename = model0.replace(' ','_') + '.html'
  cachedpath = "cache/admistore/" + brand
  c2n = {
    'Laptop':['8', 'ordinateur-portable'],
    'Desktop': ['4','ordinateur-de-bureau'],
    'Workstation': ['18','station-de-travail'],
    'Pad': ['10','tablette'],
    'Smartphone': ['393','smartphone'],
    'Monitor': ['355','ecran'],
    'AllInOne': ['5','tout-en-un']
  }
  url = "https://admistore.fr/recherche?search_query=" + brand.lower() + "+" + model.replace(' ','+')
  if category in c2n:
    cn = c2n[category][0]
    url = url + "#elasticsearch_category_" +cn+"="+cn
  else:
    print('unknown cat', category)
    return None
  try:
    with open(cachedpath + '/' + filename) as f:
      print('used cached version', (cachedpath + '/' + filename), "of", url )
      source = f.read()
  except:
    
    print('download',url)
    try:
      source = requests.get(url).text
      os.makedirs(cachedpath, exist_ok=True)
      with open(cachedpath + '/' + filename, "w") as f:
        f.write(source)
    except Exception as e:
      print('failed to download', e)
      return None
  soup = BeautifulSoup(source, 'html5lib')
  if soup.find('div', class_='noResults-SR'):
    print('no results 1')
    return None
  results = soup.find('ul',class_='product_list')
  if not results:
    print('no results 2')
    return None
  products = results.find_all('li',class_='ajax_block_product')
  # print(prices)
  price_list = []
  for p in products:
    link = p.find('a', class_='product-name').get('href')
    if re.search('https:\/\/admistore.fr\/[^\/]*'+c2n[category][1], link) and not re.search('https:\/\/admistore.fr\/[^\/]*accessoire', link):
      print( p.find('a', class_='product-name').get('href') )
      i = p.find('span',class_='price')
      if i:
        price_str = i.text.strip().replace(' ','').replace(',','.').replace('$','').replace('€','')
        print("convert",price_str)
        price = float(price_str)
        price_list.append(price)
    #   else:
    #     print('  skip 2')
    # else:
    #   print('  skip 1', p)
  # print(price_list)
  if len(price_list)>0:
    return int(np.median(np.array(price_list)))
  else:
    return None

def get_price(brand, model, category):
  p1 = get_price_shi(brand, model)
  p2 = get_price_admistore(brand, model, category)
  if p1 and p2:
    if abs(p1-p2)*10 > (p1+p2):
      print('  DIFFERENT PRICES :', p1, 'vs', p2)
    return (p1+p2)/2
  elif p1:
    return p1
  return p2

if __name__ == "__main__":

  ap = argparse.ArgumentParser(description="This routine extract the price of the model passed in parameters")

  ap.add_argument("-b", "--brand", required=True)
  ap.add_argument("-m", "--model", required=True)
  ap.add_argument("-c", "--category", required=True)
  
  args = vars(ap.parse_args())

  res = get_price(args['brand'], args['model'], args['category'])

  print(res)
