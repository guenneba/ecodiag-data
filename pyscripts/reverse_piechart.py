# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

import cv2
import numpy as np
import argparse
import json
import math
import copy
import re
import os
import sys

from numpy.core.arrayprint import printoptions
import pytesseract

def rgb2int(a):
  return (a[2] << 16) + (a[1] << 8) + a[0]

def bgr2int(a):
  return (a[0] << 16) + (a[1] << 8) + a[2]

def int2rgb(a):
  return np.array([(a>>0)&0xff, (a>>8)&0xff, (a>>16)&0xff])

def distint2(a,b):
  # print(a," ",b)
  a = int2rgb(a)
  b = int2rgb(b)
  # print(a," ",b)
  return np.max(np.abs(a-b))



#################################
#################################
def create_legend_from_ocr(input_img, img_no_charts, profile, cimg, dbg):

  if dbg >= 1:
    print("Run create_legend_from_ocr...")
  
  img = img_no_charts.copy()

  # remove border and piecharts
  # cv2.rectangle(img, (0,0), (img.shape[1],img.shape[0]), (255,255,255), 5)
  # cv2.circle(img, (circle[0], circle[1]), int(circle[2])+10, (255,255,255), -1)


  grayimg1 = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
  if dbg >= 2:
    cv2.imshow('legend ocr: grayimg1',grayimg1)
    cv2.waitKey(0)
  # thret, th1 = cv2.threshold(grayimg1, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)
  thret, th1 = cv2.threshold(grayimg1, 200, 255, cv2.THRESH_BINARY_INV)
  
  if dbg >= 2:
    cv2.imshow('legend ocr: binarized image',th1)
    cv2.waitKey(0)

  # identify non letter pixels
  kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(6,6))
  k2 = cv2.getStructuringElement(cv2.MORPH_RECT,(5,5))
  mask1 = th1.copy()
  mask1 = cv2.erode(mask1,kernel,1)
  mask1 = cv2.dilate(mask1,kernel,1)
  mask1 = cv2.dilate(mask1,k2,1)
  
  if dbg >= 2:
    cv2.imshow('legend ocr mask1',mask1)
    cv2.waitKey(0)

  # remove non letter pixels
  th1[mask1==255] = 0

  # remove noise
  # k3 = cv2.getStructuringElement(cv2.MORPH_RECT,(2,2))
  # th1 = cv2.erode(th1,k3,1)
  # th1 = cv2.dilate(th1,k3,1)

  if dbg >= 2:
    cv2.imshow('legend ocr: input of dilate',th1)
    cv2.waitKey(0)

  rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (18, profile['vertical dilation']))
  dilation = cv2.dilate(th1, rect_kernel, iterations = 1)

  if dbg >= 2:
    cv2.imshow('legend ocr: input of findContours',dilation)
    cv2.waitKey(0)

  im2 = img.copy()

  contours, hierarchy = cv2.findContours( dilation, cv2.RETR_LIST,  
                                          cv2.CHAIN_APPROX_NONE)

  # get a copy without letters for color identification
  im4color = im2.copy()
  # im4color[mask1==0] = 255
  grayimg2 = cv2.cvtColor(im4color,cv2.COLOR_BGR2GRAY)

  if dbg>=3:
    cv2.imshow('legend ocr: input of findContours 2',grayimg2)
    cv2.waitKey(0)

  # cv2.imshow('ocr im4color',im4color)
  # cv2.waitKey(0)

  res = {}

  # print(len(contours))
  for cnt in contours: 
    x, y, w, h = cv2.boundingRect(cnt)
    if w<20:
      continue
    x += 3
    w -= 6
    
    # y+=1
    # h-=2
    cv2.rectangle(cimg, (x, y), (x + w, y + h), (255, 255, 0), 1) 
    
    # Cropping the text block for giving input to OCR 
    cropped = input_img[y:y + h, x:x + w].copy()
    # print(im2.shape, " ", y + h, " ", x+w)
      
    # Apply OCR on the cropped image 
    # cv2.imshow('legend ocr: input of pytesseract',cropped)
    # cv2.waitKey(0)
    # cv2.imwrite('tmp_ocr.jpg',cropped)
    # print(pytesseract.image_to_string('tmp_ocr.jpg'))


    custom_oem_psm_config = '--psm 6'
    fulltext = pytesseract.image_to_string(cropped, config=custom_oem_psm_config).strip()
    # print(text)
    if dbg>=3:
      cv2.imshow(fulltext,cropped)

    nLine = 0
    nbLines = len(fulltext.splitlines())
    for text in fulltext.splitlines():

      actualH = int(h / nbLines)
      actualY = int(y + h*nLine/nbLines)
      nLine += 1

      # find corresponding label
      label_out = False
      for k,v in profile['ocr patterns legend'].items():
        ret = re.search(v,text)
        if ret:
          label_out = k
          break
      if not label_out:
        print("WARNING failed to find label out for ", text)
        continue

      if label_out in res and res[label_out] > 0:
        if dbg:
          print("Skip already found label: {", text, "}")
        continue

      if dbg:
        print("   ocr found ",text, " -> ", label_out)

      # search respective color
      cv2.rectangle(cimg, (max(0,x-actualH), actualY), (x+2, actualY + actualH), (0, 0, 255), 1) 
      cropped = input_img[actualY:actualY + actualH, max(0,x-actualH):x+2]
      # binarize
      x0 = max(0,x-actualH)
      y0 = actualY
      thin = grayimg2[y0:actualY + actualH, x0:x+2].copy()
      thin = gammaCorrection(thin, 0.1) # enhence contrast
      thret, th2 = cv2.threshold(thin, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)
      # get contour
      if dbg>=3 and (not thin is None) and len(thin.shape)>=2 and thin.shape[0]>0 and thin.shape[1]>0:
        cv2.imshow('find contours in ', thin)
        cv2.imshow('find contours in th ', th2)
        cv2.waitKey(0)
      contours2, hierarchy2 = cv2.findContours( th2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
      if len(contours2)>0:
        areas = [cv2.contourArea(c)/max(1,cv2.arcLength(c,True)) for c in contours2]
        largestId = areas.index(max(areas))
        x2, y2, w2, h2 = cv2.boundingRect(contours2[largestId])
        cv2.rectangle(cimg, (x0+x2, y0+y2), (x0 + x2 + w2, y0 + y2 + h2), (0, 255, 0), 1)

        c = cropped[y2+int(h2/2),x2+int(w2/2),:]
        ci = bgr2int(c)
        if dbg:
          print("    picked color : ", c, " -> ", ci)
        res[label_out] = ci
        
      else:
        print("WARNING failed to find color picking contour for '",text,"'")

  if dbg >= 1:
    print("  res = ",res)
  
  return res

def p2f(x):
  if '%' in x:
    return float(x.split('%')[0])
  return float(x)

#################################
#################################
def percent_from_ocr(input_img, circle, profile, cimg, dbg):
  
  if dbg >= 1:
    print("Run percent_from_ocr...")
  
  malus = 0

  y0 = max(0, int(circle[1]-circle[2]*1.5))
  y1 = min(input_img.shape[0], int(circle[1]+circle[2]*1.5))

  x0 = max(0, int(circle[0]-circle[2]*1.9))
  x1 = min(input_img.shape[1], int(circle[0]+circle[2]*1.9))
  img = input_img[y0:y1, x0:x1]
  if dbg>=2:
    cv2.imshow('percent_from_ocr/in', img)
    cv2.waitKey(0)

  # select letters
  letter_method = profile.get('ocr percent letter select method')
  if letter_method and letter_method=='average':
    img_max = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
  else:
    img_max = np.max(img, 2)
  if dbg >= 2:
    cv2.imshow('percent_from_ocr/img_max',img_max)
    cv2.waitKey(0)
  mask = cv2.threshold(img_max, 70, 255, cv2.THRESH_BINARY_INV)[1]
  if dbg >= 2:
    cv2.imshow('percent_from_ocr/mask1',mask)
    cv2.waitKey(0)
  # K1 = cv2.getStructuringElement(cv2.MORPH_RECT,(2,2))
  K2 = cv2.getStructuringElement(cv2.MORPH_RECT,(20,30))
  if 'ocr percent dilate size' in profile:
    s = profile['ocr percent dilate size']
    K2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(max(1,round(s[0]*circle[2])),max(1,round(s[1]*circle[2]))))
  # mask = cv2.erode(mask,K1,1)
  # if dbg >= 2:
  #   cv2.imshow('percent_from_ocr/erode',mask)
  #   cv2.waitKey(0)
  mask = cv2.dilate(mask,K2,1)
  if dbg >= 2:
    cv2.imshow('percent_from_ocr/mask',mask)
    cv2.waitKey(0)

  # keep black pixels only
  # blackpx = cv2.threshold(img_max, 160, 255, cv2.THRESH_BINARY)[1]
  # cv2.imwrite('tmp_blackpx.png',blackpx)
  # custom_oem_psm_config = '--psm 6 --oem 1'
  # text = pytesseract.image_to_string(blackpx, config=custom_oem_psm_config).strip()
  # print("-------------------",text,"-------------------")


  contours, hierarchy = cv2.findContours( mask, cv2.RETR_LIST,  
                                          cv2.CHAIN_APPROX_NONE)

  res = {}
  for cnt in contours: 
    x, y, w, h = cv2.boundingRect(cnt)
    if w<20:
      continue
    x+=3
    w-=6
    cv2.rectangle(cimg, (x0 + x, y0 + y), (x0 + x + w, y0 + y + h), (255, 255, 0), 1) 
      
    cropped = img[y:y + h, x:x + w].copy()
    cropped = cv2.cvtColor(cropped,cv2.COLOR_BGR2GRAY)
    # cv2.convertScaleAbs(cropped, cropped, 2.2, 0)
    if 'ocr percent gamma' in profile:
      cropped = gammaCorrection(cropped, profile['ocr percent gamma'])
    else:
      cropped = gammaCorrection(cropped, 1.8)
    
    custom_oem_psm_config = '--psm 6 --oem 1'
    text = pytesseract.image_to_string(cropped, config=custom_oem_psm_config).strip()
    
    lines = text.splitlines()
    singleLine = re.sub("\n", "|", text)
    if dbg>=1:
      print("    ocr found:", singleLine)

    if len(lines)==0:
      continue
    
    if dbg>=1:
      cv2.imwrite('tmp_ocr/'+lines[0]+'_ocr.png',cropped)
    # print(pytesseract.image_to_string(lines[0]+'_ocr.png'))

    if dbg>=3:
      cv2.imshow(singleLine,cropped)
      cv2.waitKey(0)

    # find corresponding label
    label_out = False
    for k,v in profile['ocr patterns direct'].items():
      ret = re.search(v,text)
      if ret:
        if label_out != False:
          print("WARNING: found multiple labels in ", text)
          malus += 0.2
        label_out = k

        # try to find associated percentage value
        ret = re.search(v+"[^0-9sS\%]*([0-9sS]+\.?[0-9]*)\s*\%",text)
        if ret:
          try:
            rawVal = re.sub(r'(S|s)', '5', ret.group(1))
            if rawVal!=ret.group(1):
              malus += 0.1
            value = p2f(rawVal)
            print(value)
            if dbg>=2:
              print("   ocr found ", label_out, " = ", value)
            if value>100:
              print("WARNING: read percentage value", value, "for", label_out, "is greater than 100%! -> skip it.")
            else:
              if label_out in res:
                print("WARNING: found duplicate for \"", label_out, "\" with values ", res[label_out], " and ", value, " (keep smallest)")
                malus += 0.2
                if value < res[label_out]:
                  res[label_out] = value  
              else:
                res[label_out] = value
          except Exception:
            print("WARNING: cannot convert " + ret.group(1) + " to float for ", label_out)
        else:
          print("WARNING: cannot find associated percentage for", label_out)
        
    if not label_out:
      print("WARNING failed to find label out for ", singleLine)
      continue

    if len(lines) < 2:
      print("WARNING: found a single line for ", singleLine)
  
  if dbg >= 1:
    print("  res = ",res)
  
  return res, malus


def gammaCorrection(src, gamma):
  invGamma = 1 / gamma

  table = [((i / 255) ** invGamma) * 255 for i in range(256)]
  table = np.array(table, np.uint8)

  return cv2.LUT(src, table)

def missingPart(data):
  if (not 'use' in data) and ('prod' in data) and ('transp' in data):
    return 'use'
  if (not 'transp' in data) and ('use' in data) and ('prod' in data):
    return 'transp'
  if (not 'prod' in data) and ('use' in data) and ('transp' in data):
    return 'prod'
  return False

#################################
#################################
class PiechartReverser:
  def __init__(self, profileFile):
    with open(profileFile) as f:
      self.profiles = json.load(f)

    # turn [r,g,b] to packed 32 bits integers
    for pk,p in self.profiles['profiles'].items():
      # print(pk)
      if 'map' in p:
        for ck,c in p['map'].items():
          p['map'][ck] = rgb2int(c)
          # print(rgb2int(c))

  def analyze(self,filename,ocrprofile=False,debug=0):

    profiles = copy.deepcopy(self.profiles)
    # print(profiles)

    input_img = cv2.imread(filename,cv2.IMREAD_COLOR)
    # img = cv2.medianBlur(input_img,5)
    img = input_img.copy()
    
    grayimg = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    grayimg = gammaCorrection(grayimg, 0.5)
    grayimg = cv2.blur(grayimg,(5,5))
    
    gradx = cv2.Sobel(grayimg,cv2.CV_32F,1,0,ksize=5)
    grady = cv2.Sobel(grayimg,cv2.CV_32F,0,1,ksize=5)

    mindim = min(img.shape[0],img.shape[1])
    maxrad = max(1, int(mindim/2))

    if debug >= 3:
      cv2.imshow("circle input", grayimg)
      cv2.waitKey(0)

    circles = cv2.HoughCircles(grayimg,cv2.HOUGH_GRADIENT_ALT,dp=1,
                                minDist=max(1,int(mindim/10)),
                                # param1=50,
                                # param2=100,
                                param1=50, # 50
                                param2=0.95, # 50
                                minRadius=10,maxRadius=maxrad)

    if debug >= 1:
      print("Circles:")
      print(circles)
    
    cimg = img.copy()

    if circles is not None:
      circles = np.uint16(np.around(circles))
      # print(circles)

      # remove text
      img_max = np.max(img, 2)
      mask = cv2.threshold(img_max, 70, 255, cv2.THRESH_BINARY_INV)[1]
      kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
      mask = cv2.dilate(mask,kernel,1)
      img = cv2.inpaint(img, mask, 20, cv2.INPAINT_TELEA)
      if debug >= 2:
        cv2.imshow('inpainted',img)
        cv2.waitKey(0)

      # inds = np.where(mask==255)
      # inds = np.asarray(inds).T
      # print(inds)
      # for xy in inds:
        # print(xy)
        #img[xy[0],xy[1],:] = 255
        # u,c = np.unique(, ,return_counts=True)

      # img[mask==255] = 255

      # cv2.imshow('mask',cv2.cvtColor(img,cv2.COLOR_BGR2GRAY))
      # cv2.waitKey(0)

      img_id = np.left_shift(img[:,:,0].astype(np.int32),16) + np.left_shift(img[:,:,1].astype(np.int32),8) + img[:,:,2].astype(np.int32)
      if debug >= 2:
        cv2.imshow('mask',mask)
        cv2.waitKey(0)


      res = {}

      if not ocrprofile:
        if len(circles) >= 2:
          ocrprofile = 'DELL'
        else:
          ocrprofile = 'HP'

      if debug:
        print("Default OCR profile: " + ocrprofile)

      ##############################
      # prepare image for OCR legend extraction
      img_no_charts = input_img.copy()
      # cv2.imshow('img_no_charts start',img_no_charts)
      # cv2.waitKey(0)
      # remove border (TODO HP only ?)
      cv2.rectangle(img_no_charts, (0,0), (img_no_charts.shape[1],img_no_charts.shape[0]), (255,255,255), 5)
      # remove piecharts
      rmin0=img_no_charts.shape[1]
      rmin1=img_no_charts.shape[0]
      dmin = min(img_no_charts.shape[0], img_no_charts.shape[1])
      rmax0=0
      rmax1=0
      offset = 0
      first = True
      for circle in circles:
        circle = circle[0]
        print(circle)
        # filter very small circles (noise)
        # print(circle[2], " > ", dmin*5)
        if first or circle[2] > 50: # filter noise
          rmax0 = max(rmax0, circle[0]+int(circle[2])+offset)
          rmax1 = max(rmax1, circle[1]+int(circle[2])+offset)
          rmin0 = min(rmin0, circle[0]-int(circle[2])-offset)
          rmin1 = min(rmin1, circle[1]-int(circle[2])-offset)
          first = False
      rmax1 -= 30
      print(img_no_charts.shape)
      print(rmin0,rmin1, rmax0,rmax1)
          # cv2.circle(img_no_charts, (circle[0], circle[1]), int(circle[2])+10, (255,255,255), -1)
      cv2.rectangle(img_no_charts, (rmin0,rmin1), (rmax0,rmax1), (255,255,255), -1)
      for circle in circles:
        circle = circle[0]
        if circle[2] > dmin/4:
          cv2.circle(img_no_charts, (circle[0], circle[1]), int(circle[2])+10, (255,255,255), -1)

      autolegend = False
      mainprofile=profiles['profiles'][ocrprofile]
      if 'ocr patterns legend' in mainprofile:
        autolegend = create_legend_from_ocr(input_img, img_no_charts, mainprofile, cimg, debug)
      # if autoprofile:
        # print(mainprofile)
        # profiles['profiles']['auto'] = {'map': autoprofile, 'opt': mainprofile['opt']}
        # if 'color_th' in mainprofile:
          # profiles['profiles']['auto']['color_th'] = mainprofile['color_th']
        # print(profiles['profiles']['auto'])
      ##############################

      # for each pie-chart
      circle_count = -1
      radius0 = 0
      for circle in circles[:min(2,len(circles))]:
        # print(circle)
        circle = circle[0]
        circle_count+=1
        if circle_count==0:
          radius0 = circle[2]

        if debug:
          # draw the outer circle
          cv2.circle(cimg,(circle[0],circle[1]),circle[2],(0,255,0),2)
          # draw the center of the circle
          cv2.circle(cimg,(circle[0],circle[1]),2,(0,0,255),3)

        # check that the circle is within the image
        if (circle[0]-circle[2]<0) or (circle[1]-circle[2]<0) or (circle[0]+circle[2]>=img.shape[1]) or (circle[1]+circle[2]>=img.shape[0]):
          cv2.circle(cimg,(circle[0],circle[1]),2,(0,255,255),3)
          print("skip clamped circle", circle)
          continue

        if circle_count > 0 and circle[2] < radius0*0.3:
          print("skip too small circle", circle)
          continue

        ##############################
        # compute the donut radii
        #
        # first, check gradient direction to see if we should shrink or enlarge
        nsamples = 100
        cumsign = 0
        for i in range(nsamples):
          angle = float(i)/float(nsamples)*math.pi
          egx = math.cos(angle)
          egy = math.sin(angle)
          px = circle[2]*egx + circle[0]
          py = circle[2]*egy + circle[1]
          gx = gradx.item(int(py),int(px))
          gy = grady.item(int(py),int(px))
          s = math.sqrt(gx*gx+gy*gy)
          if s>0:
            gx = gx/s
            gy = gy/s
            cumsign += egx*gx+egy*gy
        cumsign /= nsamples
        # print("cumsign ",cumsign)
          
        if cumsign>0:
          re = float(circle[2]-4)
          ri = 2*re/3
        else:
          ri = float(circle[2]+4)
          re = 3*ri/2
        # re2 = re*re
        # ri2 = ri*ri

        cv2.circle(cimg,(circle[0],circle[1]),int(ri),(0,0,128),1)
        cv2.circle(cimg,(circle[0],circle[1]),int(re),(0,0,255),1)
        ##############################

        c = 0
        # hist = {}

        mask = np.zeros(grayimg.shape, np.uint8)
        cv2.circle(mask,(circle[0],circle[1]),int(re),255, -1)
        cv2.circle(mask,(circle[0],circle[1]),int(ri),0, -1)

        (u,c) = np.unique(img_id[mask==255],return_counts=True)
        ind = np.lexsort( (u,c) )
        ind = np.flip(ind)
        freq = np.asarray((u[ind], c[ind])).T
        
        # print(ind)
        # (u,c) = np.unique(img_id,True)
        # dtype=[('u',int),('c',int)]
        
        # freq = np.sort(freq)

        if debug:
          # print(freq[0:10,:])
          for i in freq[0:20]:
            # print(i)
            print(int2rgb(i[0]), " : ", i[0], " : ", i[1])

        # X_masked = np.ma.masked_array(img_id, mask==255)
        # A = img_id[:]
        # B = mask[:]
        # print(A.size)
        # print(np.sum(B==255))
        # X_select = A[B==255]
        # print(X_select.size)
        # u = np.unique(X_select,False)
        # print(img_id[mask==1])
        # if debug:
          # print(freq[0:10,:])

        # cm = freq[0,0]
        # print(img[img_id==cm][0,0])


        # find best matching profile
        # print(profiles)

        
        try_ocr = True
        total_px = np.sum(freq[:,1])
        total_pixels = np.sum(freq[:,1])
        trueSum = 0

        # try using the extracted legend
        if autolegend:
          score = 0
          count_opt = 0
          sum_pixels = 0
          opt = mainprofile['opt']
          i=0
          if debug:
            print("\n--- auto legend ---")
            print(autolegend)
            print("--- frequencies ---")
            print(freq)
            print("---\n")
          for ck,c in autolegend.items():
            # score -= 1
            if c in freq[:,0]:
              score += 1
              ind = np.where(freq[:,0]==c)[0]
              sum_pixels += np.sum(freq[ind,1])
              print("add ", ck, c)
            elif ck in opt:
              count_opt +=1
            i += 1
          
          score += float(sum_pixels)/float(total_pixels)
          
          print("autolegend scoring: {}+{} / {}".format(score, count_opt, len(autolegend)))
        
          # if score < len(autolegend):
            # print(autolegend)

          
          if score > 0:
            if debug:
              print(" -> clean up unique color ")

            # clean up unique colors
            # print(np.unique(freq[:,0]).size)

            count = 0
            sum = 0
            try:
              dist_th = mainprofile['color_th']
            except:
              dist_th = 20
            for r in freq:
              # print (r)
              a = r[0]
              best_dist = 1e9
              best_c = 0
              if not a in autolegend:
                for ck,c in autolegend.items():
                  d = distint2(a,c)
                  if d<best_dist:
                    best_dist = d
                    best_c = c
                    if d < dist_th:
                      r[0] = c
                if best_dist > dist_th:
                  if debug and r[1] > 2:
                    print("skip ", best_dist, " : ", int2rgb(a), int2rgb(best_c), " (", r[1], ")")
                  if r[1] > 0.05*total_px: # > 5%, arbitrary
                    count += 1
                    sum += r[1]
            if debug:
              tmp = {}
              for r in freq:
                if r[1]>dist_th:
                  if not r[0] in tmp:
                    tmp[r[0]] = 0
                  tmp[r[0]] += r[1]
              print(freq[0:10,:])
              print(tmp)
              # print("nb unique: ",np.unique(freq[:,0]).size)

            names = {}
            for ck in autolegend.keys():
              names[ck] = ck
            
            inserteditems = []
            trueSum = 0
            for ck,c in autolegend.items():
              ind = np.where(freq[:,0]==c)[0]
              item_name = ck
              if names and ck in names:
                item_name = names[ck]
              if ind.size>0 :
                # print(item_name , " -> " , freq[ind[0],1])
                q = np.sum(freq[ind,1])
                if trueSum==0 or ('opt' in mainprofile and item_name not in mainprofile['opt']) or float(q)/float(total_px)>0.0015: # filter noise
                  res[item_name] = q
                  trueSum += q
                  count += 1
                  inserteditems.append(item_name)
                elif debug:
                  print("NOTE skip noise: ", item_name, " with ", q, " pixels (", float(q)/float(total_px)*100, "%)")
              # else:
                # res[item_name] = 0
            
            if trueSum/total_px > 0.5:

              print("GOOD ", trueSum, "/" , total_px)

              try_ocr = False
              sum += trueSum

              is_details_pie_charts = ('profile/main' in res and res['profile/main'] == 'ocr')
              if (not is_details_pie_charts) and (not 'EOL' in res):
                print("WARNING: missed end-of-life, add it explicitely...")
                res['EOL'] = 0
                count+=1
                inserteditems.append('EOL')
                res['extrapolated'] = 'EOL'

              # correction & normalization:
              # total = np.sum(freq[3:,1])
              # print(total, ' vs ', np.sum(freq[:,1]))
              corr = 0
              if count > 0:
                corr = float(total_px-sum)/float(count)
                # bound correction to 1% ?
                corr = min(corr, total_px*0.01)
                total_px = sum + corr * count
              factor = 1

              if is_details_pie_charts and 'prod' in res:
                factor = res['prod']/100.
              for ck,c in res.items():
                if ck in inserteditems:
                  res[ck] = round(10000*(c+corr)/total_px*factor)/100.

              if 'profile/main' in res:
                res['confidence2'] = round(100*trueSum/total_px)/100.
                res['profile/prod'] = 'auto legend'
              else:
                res['confidence1'] = round(100*trueSum/total_px)/100.
                res['confidence2'] = 1
                res['profile/main'] = 'auto legend'
            else:
              # cleanup
              for k in inserteditems:
                res.pop(k)

            # print(res)
          
        if try_ocr:
          if debug:
            print("No profile is good enough, we should have trueSum/total_px = ", trueSum/total_px, " > ", 0.5)
          if not res and 'ocr patterns direct' in mainprofile:
            res, malus = percent_from_ocr(input_img, [circle[0],circle[1],re], mainprofile, cimg, debug)
            if res:
              is_details_pie_charts = ('profile/main' in res and res['profile/main'] == 'ocr')

              # remove possible wrong entries in auto profile
              if autolegend:
                print(autolegend)
                for k in res:
                  if k in autolegend:
                    autolegend.pop(k)
                print(autolegend)

              res['confidence1'] = 0
              for k in ['use', 'prod', 'transp', 'EOL']:
                if k in res:
                  res['confidence1'] += res[k]
              res['confidence1'] /= 100
              res['confidence1'] -= malus

              if not is_details_pie_charts:
                part = missingPart(res)
                if part:
                  print("WARNING: missed ", part, ", add it explicitely...")
                  if not 'EOL' in res:
                    print("WARNING: missed end-of-life, add it explicitely...")
                    res['EOL'] = 0.5 # arbitrary
                  res[part] = 0
                  res[part] = 100 - (res['use']+res['prod']+res['transp']+res['EOL'])
                  res['extrapolated'] = part
                else:
                  if not 'EOL' in res:
                    if 'use' in res and 'prod' in res and 'transp' in res:
                      res['EOL'] = max(0, 100 - (res['use']+res['prod']+res['transp']))
                    else:
                      res['EOL'] = 0.5 # arbitrary
                    print("WARNING: missed end-of-life, add it explicitely... (", res['EOL'], ")")
                    res['extrapolated'] = 'EOL'
              
              res['profile/main'] = 'ocr'
              res['confidence2'] = 1
          # exit(1)
        # for y in range(circle[0]-circle[2],circle[0]+circle[2]):
        #   for x in range(circle[1]-circle[2],circle[1]+circle[2]):
        #     d = [y-circle[0],x-circle[1]]
        #     d2 = np.inner(d,d)
        #     if d2<re2 and d2>ri2:
        #       c += 1
        #       cimg.itemset(x,y, 0,255)

      # print(c)

      if debug >= 2:
        cv2.imshow('detected circles',cimg)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

      return res


if __name__ == "__main__":

  ap = argparse.ArgumentParser()

  ap.add_argument("-i", "--input", required=True, help="input image file")
  ap.add_argument("-o", "--ocrprofile", required=False, default='', type=str, help="profile for OCR")
  ap.add_argument("-d", "--debug", required=False, default=0, type=int, help="show circles and debug info")
  
  args = vars(ap.parse_args())

  pr = PiechartReverser(os.path.join(sys.path[0], "profiles.json"))
  res = pr.analyze(args['input'] , args['ocrprofile'], args['debug'])
  
  if res:
     print(res)
  else:
    print("no good match")
    exit(1)
  
  