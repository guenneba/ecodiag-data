# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse

import pandas as pd
import numpy as np
import math
import re
import datetime
from datetime import datetime
import json
import numbers

def areApprox(a,b,tol=0.01):
  return abs(a-b) < max(a,b)*tol

def p2f(x):
  if type(x) == str and '%' in x:
    return float(x.strip('%'))
  return float(x)

def extract(r, txt):
  res = re.search(r,txt)
  if res:
    return res.group(1).strip()
  else:
    return ''

def compAndReport(a,b,label,bFactor):
  bval = p2f(b)*bFactor
  if not areApprox(p2f(a), bval, 0.07): # tolerate a 7% relative error
    if not math.isnan(bval) and not math.isnan(a):
      print(label + ' are different: ' + str(a) + ' vs ' + str(bval))
      return False
  return True

def compstrAndReport(a,b,label):
  aval = a
  bval = b
  if isinstance(a,str):
    aval = a.replace('”','in')
    aval = re.sub(r'\s\s+', ' ', aval).strip()
  if isinstance(b,str):
    bval = re.sub(r'\s\s+', ' ', bval).strip()
  if (aval != bval) and (not ((not isinstance(a,str)) and  (not isinstance(b,str)) and math.isnan(a) and math.isnan(b))):
    print(label + ' are different: "' + str(aval) + '" vs "' + str(bval) + '"')
    return False
  return True

def isEmpty(x):
  return isinstance(x,str) and x=='' or not isinstance(x,str) and math.isnan(x)

def toInt(x):
  try:
    if isinstance(x, str):
      x = float(x)
    if x > 0:
      i = int(x+0.5)
    else:
      i = int(x-0.5)
    return i
  except:
    return x

def is_newer(a,b):
  try:
    da = datetime.strptime('{} {}'.format(a.month, int(a.year)), '%B %Y')
    db = datetime.strptime('{} {}'.format(b.month, int(b.year)), '%B %Y')
    return da > db
  except:
    return False

def cleanupHP(df):

  main_percentages = ['use_p', 'prod_p', 'transp_p', 'EOL_p']
  detail_percentages = []
  for k in df.columns:
    if k.endswith('_p') and not k in main_percentages:
      detail_percentages.append(k)

  # compute electricity factor assuming 100% of the 'use' phase comes from the electricity consumption,
  # which is not always the case (e.g., maintenance, etc.). This is typically the case for printers.
  def elecfactor(d):
    return (df['use_p'] * df['global_mean'] / (100 * df['lifetime'] * df['kWh'])).round(3)
  df['elecfactor'] = elecfactor(df)

  df.loc[df['extrapolated'].isna(),'extrapolated'] = ''

  ############################################################
  # HP fixes
  #
  # Problem #1: for some items, the reported lifetime does not
  # match the one used for computation, leading to odd elecfactor.
  # Those can be identified by computiong what would have been the
  # lifetime for a factor of 0.686kgCO2e/kWh and checking this
  # number is roughly an integer
  # mask1 = (df['brand'] == 'HP') # & (~ (df['use'].isna() | df['lifetime'].isna() | df['kWh'].isna()))
  #lifetime = df.loc[mask1,'elecfactor']*df.loc[mask1,'lifetime']/0.686

  for idealFactor in [0.686, 0.525]:
    lifetime = df['elecfactor']*df['lifetime']/idealFactor
    mask = (df['brand'] == 'HP') & ((df['elecfactor']<0.52)|(df['elecfactor']>0.695)) & (~df['elecfactor'].isna()) & (abs(lifetime-df['lifetime'])>0.6) & (abs(lifetime-lifetime.round())<0.1)
    df.loc[mask,'lifetime'] = lifetime.loc[mask].round()
    df.loc[mask,'extrapolated'] += ' lifetime'
    # recompute elecfactor
    df['elecfactor'] = elecfactor(df)
  
  # df['lt'] = lifetime
  # print(df.loc[ (abs(lifetime-lifetime.round())<0.09) & (df['model']=='Zbook 17 G5 Mobile Workstation'),'lt'])
  # quit()

  # Problem #2: for many items, there is a odd discrepancy between
  # the reported CO2e values (mean & percentiles) and the mean obtained
  # by summing up the values of the bar plots. For those items, we can
  # also observe that the percentages of the pie-chart match the percentage
  # of the bar plot. Moreover, the 'use' phase of the bar-plot can also be
  # properly recovered from "kWh * years * 0.525kgCO2e/kWh"
  # -> Those observations suggest that the bar plots are corrects,
  #    but that the global mean (and percentiles) are not correct.
  # -> It is important to correct this error as it also impact the embodied values.
  # Those items can be found from the estimated electricity factor which is around 0.42
  mask = (df['brand'] == 'HP') & (df['elecfactor'] > 0.34) & (df['elecfactor'] < 0.46)
  factor = 0.525 / df.loc[mask,'elecfactor']
  for k in ['global_mean','global_stdev','global5p','global95p'] + main_percentages + detail_percentages:
    k1 = k.replace('_p','')
    df.loc[mask,k1] *= factor
  # recompute elecfactor
  df['elecfactor'] = elecfactor(df)
  df.loc[mask,'extrapolated'] += ' global_mean'
  ############################################################
  return df

if __name__ == "__main__":

  ap = argparse.ArgumentParser()

  ap.add_argument("-i", "--input", required=True, help="input .csv file")
  ap.add_argument("-b", "--boavista", required=True, help="input boavista's .csv file")
  ap.add_argument("-o", "--output", required=False, help="output merged .csv file")
  
  args = vars(ap.parse_args())


  csv_delim = ','
  
  data = pd.read_csv(args['input'], delimiter=csv_delim)
    # converters={'use_p':p2f,'use_p':p2f,'prod_p':p2f,'transp_p':p2f,'eol_p':p2f,
                # 'box_p':p2f,'HDD_p':p2f,'optical_drive_p':p2f,'power_p':p2f,'board_p':p2f,'disp_p':p2f,'packaging_p':p2f} )
  data.rename(columns = lambda x: x.strip(), inplace = True)
  data = cleanupHP(data)


  boa = pd.read_csv(args['boavista'], delimiter=csv_delim)
  boa.rename(columns=lambda x: x.strip(), inplace = True)

  # print("Input:")
  # print(data.columns)
  # print(data.dtypes)

  # print("\n\nBoavizta:")
  # print(boa.columns)
  # print(boa.dtypes)
  
  # [ 'manufacturer', 'name', 'category', 'subcategory', 'gwp_total', 'gwp_use_ratio', 'yearly_tec', 'lifetime', 'use_location',
  #   'report_date', 'sources', 'gwp_error_ratio', 'gwp_manufacturing_ratio', 'weight', 'assembly_location', 'screen_size',
  #   'server_type', 'hard_drive', 'memory', 'number_cpu', 'height', 'added_date', 'add_method']

  # ,brand,model,month,year,global_mean,global_stdev,weight,lifetime,useloc,kWh,model2,category,source,filename,confidence1,
  # extrapolated,profile/main,confidence2,profile/prod,prod_p,use_p,EOL_p,transp_p,packaging_p,board_p,power_p,HDD_p,box_p,prod,
  # use,EOL,transp,packaging,board,power,HDD,box,size,disp_p,optical_drive_p,disp,optical_drive,SSD_p,SSD,paia,battery_p,battery,
  # materials_p,lcd_assembly_p,IC_p,materials,lcd_assembly,IC,mechanical_p,PWBs_p,other_electronics_p,mechanical,PWBs,
  # other_electronics,housing_p,electronics_p,panel_p,housing,electronics,panel,assembly_p,assembly,global5p,global95p
  toBoa = {
    'brand': 'manufacturer',
    'model': 'name',
    'category': 'subcategory',
    'global_mean': 'gwp_total',
    'use_p': 'gwp_use_ratio', # factor 100
    'kWh': 'yearly_tec',
    'lifetime': 'lifetime',
    'useloc': 'use_location',
    'year': 'report_date', # extract year
    'month': 'report_date', # extract month
    'source': 'sources',
    'global_stdev': 'gwp_error_ratio', # convert to absolute val
    'prod_p': 'gwp_manufacturing_ratio', # factor 100
    'weight': 'weight',
    'size': 'screen_size',
    'conf_server_type': 'server_type',
    'conf_storage': 'hard_drive',
    'conf_DRAM': 'memory',
    'conf_nb_cpus': 'number_cpu',
    'assemblyloc': 'assembly_location',
    'extract_date': 'added_date'
  }

  # inverse map
  fromBoa = {}
  for k,v in toBoa.items():
    fromBoa[v] = k

  # This function cleans up model names by:
  # - removing the brand and other useless words
  # - unifying "2-in-1" spelling
  # - clients -> client
  # - removing double spaces
  def cleanUpModel(df,m):
    df[m] = df[m].replace(np.nan, '', regex=True)
    df = df.astype({m: str})
    df[m] = df[m].apply(lambda x: re.sub(r'(?i:^Dell |^HP |^Lenovo | monitor| display| notebook PC| PC| desktop| datasheet|&#34;)', '', x).strip())
    df[m] = df[m].apply(lambda x: re.sub(r'(?i:2-in-1)', '2-in-1', x))
    df[m] = df[m].apply(lambda x: re.sub(r'(?i:client)s', '\1', x))
    df[m] = df[m].apply(lambda x: re.sub(r'\s\s+', ' ', x))
    return df
    

  ############################################################
  # cleanup Boavizta database
  
  # from observation, when duplicates exist, the last entry is the most complete one,
  # so let's not bother and simply remove the first entries
  boa.drop_duplicates(subset=['name'], keep='last', inplace=True)

  boa.rename(columns = {'category': 'tier'}, inplace = True)
  boa.rename(columns = fromBoa, inplace = True)

  # Update percentage values (0.05 => 50)
  for k in boa.columns:
    if k.endswith('_p'):
      boa[k] *= 100
  boa['global_stdev'] = (boa['global_stdev'] * boa['global_mean']).round()
  # Split year and month
  boa['year'] = boa['month'].apply(lambda x : extract(r'(2[0-9]{3})', x))
  boa['month'] = boa['month'].apply(lambda x : extract(r'([A-Za-z]+)', x))
  # Unify spelling
  boa.loc[boa['brand']=='Dell','brand'] = 'DELL'
  # Some server items have a 'size' of 4...??? -> remove those '4'
  boa.loc[boa['category']=='Server','size'] = np.nan
  # For some smartphones, the 'size' is in pixels:
  mask = (boa['category']=='Smartphone') & (boa['size']>100)
  boa.loc[mask,'pixel_size'] = boa.loc[mask,'size']
  boa.loc[mask,'size'] = np.nan
  # clean up model names
  boa = cleanUpModel(boa, 'model')
  # boa['model'] = boa['model'].apply(lambda x: x.replace('Edition','').strip())

  # HP Servers -> HPE
  boa.loc[(boa['brand']=='HP') & (boa['category']=='Server') ,'brand']

  boa.loc[(boa['model'].str.contains('AIO')) ,'category'] = 'AllInOne'
  boa.loc[(boa['model'].str.contains(r'(?i)all.in.one', regex=True)) ,'category'] = 'AllInOne'
  

  boaOthers = []
  for k in boa.columns:
    if not k in toBoa:
      boaOthers.append(k)
  ############################################################


  ############################################################
  # cleanup ED database

  data.loc[data['useloc']=='Worldwide','useloc'] = 'WW'
  data.loc[data['useloc']=='Global','useloc'] = 'WW'
  data.loc[data['useloc']=='Default','useloc'] = 'WW'
  data.rename(columns = {'Unnamed: 0':'info'}, inplace = True)
  data.drop_duplicates(subset=data.columns.drop(['info','source','filename']), keep='last', inplace=True, ignore_index=True)
  data = cleanUpModel(data, 'model')
  data = cleanUpModel(data, 'model2')
  data.loc[data['model2']=='Download the PDF', 'model2'] = ''
  data.loc[data['model2']=='Download Whitepaper', 'model2'] = ''
  data['conf_storage'] = data['conf_storage'].replace(r'X([1-9]) ', r'x\1 ', regex=True)
  data.loc[(data['model'].str.contains('AIO')) ,'category'] = 'AllInOne'
  data.loc[(data['model'].str.contains(r'(?i)all.in.one', regex=True)) ,'category'] = 'AllInOne'
  ############################################################
  
  # add pure boavizta's columns with a "boa_" prefix
  def addBoaviztaAttribs(el, row):
    for k1 in boaOthers:
      el['boa_'+k1] = getattr(row, k1)
    return el

  # pure boavizta's item
  def createRowFromBoavizta(row):
    el = {}
    el['info'] = 'BOA'
    for k1 in toBoa.keys():
      el[k1] = getattr(row,k1)
    el = addBoaviztaAttribs(el, row)
    return el

  data_out = pd.DataFrame(columns=data.columns)

  mask = np.ones(data.shape[0], dtype=bool)
  for brow in boa.itertuples(index=True, name='Pandas'):

    search_res = data.loc[data['model']==brow.model]

    cmp = lambda prop, boaFactor=1: compAndReport(search_res[prop].array[0], getattr(brow,prop), prop, boaFactor)
    cmpstr = lambda prop: compstrAndReport(search_res[prop].array[0], getattr(brow,prop), prop)

    newel = {}
    # search_res = search_res.dropna()
    if search_res.size > 0:
      newel = search_res.iloc[0].copy()
      # print(newel)
      # if search_dup.size > 0:
      idx = newel.name
      if mask[idx]:
        mask[idx] = False
        # print(search_res['model'].array[0])
        # print(brow)
        # print(brow[toBoaIdx['global_mean']+1])
        OK = True
        notOKs = []
        numerical_attribs = ['global_mean', 'global_stdev', 'use_p', 'kWh', 'lifetime', 'prod_p', 'weight', 'size', 'conf_DRAM', 'conf_nb_cpus']
        str_attribs = ['conf_server_type', 'conf_storage']
        for k in numerical_attribs:
          if not cmp(k):
            notOKs.append(k)
            if k != 'use_p':
              OK = False
        for k in str_attribs:
          if not cmpstr(k):
            notOKs.append(k)
            OK = False

        ok_info = 'OK'
        
        if notOKs == ['use_p']:
          # In Boavizta, for many entries, 'use_p' is estimated from the TEC using 0.475 kgCO2e/kWh as carbon intensity factor of the electricity.
          # However, the 'use_p' value recovered using this 0.475 factor does not match piecharts percentages.
          # So, if the two entries differ only about 'use_p' and that the Boavizta's 'use_p' clearly comes from this wrong assumption, then we skip the Boavizta entry.
          if areApprox(getattr(brow,'global_mean') * getattr(brow,'use_p') / 100 / getattr(brow,'lifetime') / getattr(brow,'kWh') , 0.475, 0.001):
            ok_info = 'xuse'
            print('Ignore wrongly extrapolated use_p from Boavizta (', brow.brand, brow.model, ')')
          else:
            OK = False
        
        if 'builtin' in str(newel['profile/main']):
          print(' -> Ignore the item (', brow.brand, brow.model, ') from Boavizta because the ecodiag data has been manually checked\n')
          ok_info = 'KO handcoded'
          OK = True
        elif newel['brand'] == 'HP' and 'global_mean' in newel['extrapolated']:
          print(' -> Ignore the item (', brow.brand, brow.model, ') from Boavizta because the original data has been explicitly fixed by ecodiag wrt to kgCO2/kWh\n')
          ok_info = 'KO HPfix1'
          OK = True
        elif newel['brand'] == 'HP' and 'lifetime' in newel['extrapolated']:
          print(' -> Ignore the item (', brow.brand, brow.model, ') from Boavizta because the original data has been explicitly fixed by ecodiag wrt to lifetime\n')
          ok_info = 'KO HPfix2'
          OK = True
        elif newel['brand'] == 'HP' and 'productcarbonfootprint_' in brow.source:
          print(' -> Ignore the item (', brow.brand, brow.model, ') from Boavizta because the source file starting with "productcarbonfootprint_" is obsolete\n')
          ok_info = 'KO outdated source'
          OK = True
        elif newel['brand'] == 'HP' and ('OMEN' in newel['model'] or 'VICTUS' in newel['model']):
          print(' -> Ignore the item (', brow.brand, brow.model, ') from Boavizta because the model name does not reflect the variant \"', newel['model'], '\"\n')
          ok_info = 'KO HP variant'
          OK = True
        elif newel['filename'] in ['pcf-ideapad-3-17_v-17-r3.pdf', 'pcf-thinkpadX390-update2.pdf', 'e1920h-monitor-pcf-datasheet.pdf', 'c07856928.pdf', 'c08170567.pdf', 'c08079479.pdf', 'c07989120.pdf', 'c07989121.pdf', 'c07856929.pdf']:
          print(' -> Ignore the item (', brow.brand, brow.model, ') from Boavizta because the source file used by ecodiag is considered to be newer or parsing to be more correct (', newel['filename'], ')\n')
          ok_info = 'KO handchecked'
          OK = True
        elif is_newer(newel, brow):
          print(' -> Ignore the item (', brow.brand, brow.model, ') from Boavizta because the source file used by ecodiag is newer\n')
          ok_info = 'KO newer source file'
          OK = True
        
        if not OK:
          print(' -> ' + brow.brand + ' ' + brow.model + ' ' + search_res['filename'].array[0] + '\n')
          newel['info'] = 'ED KO'

          # add the Boavizta entry for record
          boael = createRowFromBoavizta(brow)
          boael['info'] = 'BOA KO'
          data_out = pd.concat([data_out, pd.DataFrame([boael])], ignore_index=True, axis=0)
        else:
          newel['info'] = 'ED (BOA '+ok_info+')'
          completed = []
          for attrib in numerical_attribs + str_attribs + ['brand', 'useloc']:
            bval = getattr(brow, attrib)
            if isEmpty(newel[attrib]) and not isEmpty(bval):
              completed.append(attrib)
              newel[attrib] = bval
              print('Fill', attrib, 'from boavizta:', bval)
          if completed:
            newel['info'] = 'ED + BOA ('+','.join(completed)+')'
            print(' -> Completed ' + brow.brand + ' ' + brow.model + ' ' + search_res['filename'].array[0] + '\n')

        newel = addBoaviztaAttribs(newel, brow)

        # print(newel)
        # data_out = data_out.append(newel, ignore_index=True)
        # break
      else:
        search_dup = data_out.loc[data['model']==brow.model]
        print('skip duplicate: ' + brow.brand + ' ' + brow.model + ' ')
        # TODO merge boavizta's data
        continue
    else:
      newel = createRowFromBoavizta(brow)
    
    data_out = pd.concat([data_out, pd.DataFrame([newel])], ignore_index=True, axis=0)
  
  # add other items (the ones that are not in boavizta's list)
  data.loc[mask,'info'] = 'ED'
  data_out = pd.concat([data_out, data.loc[mask]], axis=0, ignore_index=True)

  main_percentages = ['use_p', 'prod_p', 'transp_p', 'EOL_p']
  detail_percentages = []
  for k in data_out.columns:
    if k.endswith('_p') and not k in main_percentages:
      detail_percentages.append(k)

  # some cleanup of models
  data_out.loc[data_out['model2']==data_out['model'], 'model2'] = ''
  data_out.loc[data_out['model']=='', 'model'] = data_out.loc[data_out['model']=='', 'model2']
  mask = data_out['model'].isna()
  data_out.loc[mask, 'model'] = data_out.loc[mask, 'model2']
  data_out.loc[mask, 'model2'] = '<-'

  # compute electricity factor assuming 100% of the 'use' phase comes from the electricity consumption,
  # which is not always the case (e.g., maintenance, etc.). This is typically the case for printers.
  data_out['elecfactor'] = (data_out['use_p'] * data_out['global_mean'] / (100 * data_out['lifetime'] * data_out['kWh'])).round(3)

  # turn some columns as integer
  for k in ['year', 'global_mean', 'global_stdev', 'global5p', 'global95p']:
    data_out[k] = data_out[k].apply(toInt)
    data_out = data_out.astype({k: "Int64"})

  # limit decimals
  for k in main_percentages + detail_percentages + ['kWh','confidence1','confidence2']:
    data_out[k] = data_out[k].round(2)

  # compute sum of main percentage (for debug purposes, should be equal to 100)
  data_out['sum_percentages'] = 0
  for k in main_percentages:
    mask = ~data_out[k].isna()
    data_out.loc[mask,'sum_percentages'] += data_out.loc[mask,k]
  data_out.loc[mask,'sum_percentages'] = data_out.loc[mask,'sum_percentages'].round(1)

  # compute ratio of details percentages versus production (for debug purposes, should be equal to 1)
  data_out['ratio_prod_percentages'] = 0
  for k in detail_percentages:
    mask = ~data_out[k].isna()
    data_out.loc[mask,'ratio_prod_percentages'] += data_out.loc[mask,k]
  data_out['ratio_prod_percentages'] = (data_out['ratio_prod_percentages'] / data_out['prod_p']).round(2)

  # compute production and use values for Boavizta's entries
  for k in ['prod', 'use']:
    kp = k+'_p'
    mask = data_out[k].isna() & data_out[kp] > 0
    data_out.loc[mask,k] = (data_out.loc[mask,'global_mean'] * data_out.loc[mask,kp] / 100).round(2)

  # compute relative standard deviation
  data_out['global_stdev_ratio'] = (data_out['global_stdev'] / data_out['global_mean'] * 100).round(1)

  # compute total embodied kgCO2e
  mask = (~data_out['global_mean'].isna()) & (~data_out['use'].isna())
  data_out.loc[mask,'embodied'] = (data_out.loc[mask,'global_mean'].astype('float') - data_out.loc[mask,'use'].astype('float')).round(0)



  ############################################################
  # reorder columns
  newdetailorder = ['packaging_p', 'box_p', 'board_p', 'power_p', 'HDD_p', 'SSD_p', 'optical_drive_p', 'disp_p', 'battery_p']
  # append forgotten detail columns
  for k in detail_percentages:
    if not k in newdetailorder:
      newdetailorder.append(k)
  
  newcolumns = ['info', 'brand', 'model', 'model2', 'category', 'year', 'month',
                'weight', 'size', 'lifetime', 'useloc', 'kWh',
                'global_mean', 'global_stdev', 'global_stdev_ratio', 'global5p',	'global95p', 'embodied'] + \
               main_percentages + [x.replace('_p','') for x in main_percentages] + \
               ['conf_DRAM', 'conf_server_type', 'conf_nb_cpus', 'conf_storage'] + \
               newdetailorder + [x.replace('_p','') for x in newdetailorder] + \
               ['source', 'filename', 'md5', 'paia','assemblyloc',
                'extrapolated', 'profile/main', 'confidence1', 'profile/prod', 'confidence2', 'price'
                'boa_tier', 'boa_height', 'boa_pixel_size', 'boa_add_method', 'boa_added_date', 
                'elecfactor', 'sum_percentages', 'ratio_prod_percentages']
  
  # append forgotten columns
  for k in data_out.columns:
    if not k in newcolumns:
      newcolumns.append(k)
  
  data_out = data_out.reindex(columns=newcolumns)
  ############################################################


  data_out.to_csv(args['output'], float_format='%g')
