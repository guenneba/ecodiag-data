# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

import pandas as pd
import numpy as np
import argparse
import json
import re
import sys
import os
import subprocess
import glob
import copy
import pdf_extract
import reverse_piechart
from datetime import datetime
import hashlib
import getprice

# You need to install pdftohtml from the poppler package (or poppler-utils),
# not the legacy pdftohtml package which is based on xpdf !
# This old version cannot extract images.


def compute_md5(fname):
  # return subprocess.check_output("md5 -q \"{}\"".format(fname), shell=True).strip()
  hash_md5 = hashlib.md5()
  with open(fname, "rb") as f:
    for chunk in iter(lambda: f.read(4096), b""):
      hash_md5.update(chunk)
  return hash_md5.hexdigest()

def find_file(name, path):
  for root, dirs, files in os.walk(path):
    if name in files:
      return os.path.join(root, name)

def append_chartdata(itemdata,chartdata):
  
  # append "_p" suffix
  chartdata_copy = copy.deepcopy(chartdata)
  for ik in chartdata_copy.keys():
    if ik not in ['confidence1','confidence2','profile/main','profile/prod','extrapolated']:
      chartdata[str(ik)+"_p"] = chartdata.pop(ik)

  itemdata = { **itemdata, **chartdata }

  # print(itemdata)

  if not 'global_mean' in itemdata:
    print("WARNING no global_mean")
    return itemdata

  # compute absolute CO2e value
  total = float(itemdata['global_mean'])
  if total:
    details_sum=0
    details_sum_p=0
    main_sum=0
    main_sum_p=0
    main_components = ['use','prod','transp','EOL']
    for ik,iv in chartdata.items():
      if ik.endswith("_p"):
        val_p = float(iv)/100.
        val = round(total * val_p, 2)
        iname = re.search(r'(.*)_p',ik).group(1)
        itemdata[iname] = val
        if not iname in main_components:
          details_sum += val
          details_sum_p += val_p
        else:
          main_sum += val
          main_sum_p += val_p
      
    if not 'prod' in itemdata:
      if not 'extrapolated' in itemdata:
        itemdata['extrapolated'] = ''
      if ('use' in itemdata and 'transp' in itemdata and 'EOL' in itemdata) or (details_sum==0 and 'use' in itemdata):
        itemdata['prod'] = total-main_sum
        itemdata['prod_p'] = round(100-100*main_sum_p, 2)
        itemdata['extrapolated'] += ' "prod_p=100-others"'
      elif details_sum>0:
        itemdata['prod'] = round(details_sum, 2)
        itemdata['prod_p'] = round(details_sum_p*100, 2)
        itemdata['extrapolated'] += ' "prod_p=sum details"'
      
    if 'prod' in itemdata:
      # sanity checks
      if abs( itemdata['prod'] - (total-main_sum) ) > 0.1:
        print("WARNING prod != total-others, i.e.: ", itemdata['prod'], " != ", total-main_sum)
      if abs( itemdata['prod_p'] - (100.-100*main_sum_p) ) > 0.5:
        print("WARNING %prod != 100-%others, i.e.: ", itemdata['prod_p'], " != ", 100.-100*main_sum_p)
      if details_sum>0:
        if abs( itemdata['prod'] - (total*details_sum_p) ) > 0.1:
          print("WARNING prod != total*%prod, i.e.: ", itemdata['prod'], " != ", total*details_sum_p)

    # other sanity checks
    def check_gt(a,b):
      if a in itemdata and b in itemdata and itemdata[b] > itemdata[a]:
        print("WARNING ",b," > ",a,"   i.e., ",itemdata[b],  " > ", itemdata[a])
    
    check_gt('disp_p','power_p')
    check_gt('disp_p','SSD_p')
    check_gt('disp_p','HDD_p')
    check_gt('board_p','HDD_p')
    check_gt('board_p','packaging_p')
    check_gt('power_p','packaging_p')
    check_gt('power_p','HDD_p')
    

  return itemdata


if __name__ == "__main__":

  unpie = reverse_piechart.PiechartReverser(os.path.join(sys.path[0], "profiles.json"))

  alldata = []
  invaliddata = []
  allfilenames = []
  now = datetime.now()

  # load all file database
  files_df = False
  for f in glob.iglob("cache/*_files.csv"):
    print("Import ", f)
    tmp_df = pd.read_csv(f)
    if type(files_df) == bool and not files_df:
      files_df = tmp_df
    else:
      files_df = files_df.append(tmp_df)

  # load current state
  try:
    with open('cache/extract_all_cache.json') as f:
      for l in f.readlines():
        item = json.loads(l)
        if not 'filename' in item:
          print('no filename', item)
          item['filename'] = ''
        if not item['filename'] in allfilenames:
          if not 'md5' in item:
            filename = find_file(item['filename'], 'cache/')
            if filename:
              print('md5',filename)
              item['md5'] = compute_md5(filename)
            else:
              print('WARNING cannot find file', item['filename'], 'to update md5')
          if 'model' in item and item['model'] != 'invalid':
            alldata.append(item)
          else:
            invaliddata.append(item)
          allfilenames.append(item['filename'])
        else:
          print('SKIP duplicate', item['filename'])
    # rewrite cache
    print("rewrite cache...")
    print('press enter...')
    input()
    with open("cache/extract_all_cache.json", "w") as f:
      for i in alldata:
        f.write(json.dumps(i))
        f.write("\n")
      for i in invaliddata:
        f.write(json.dumps(i))
        f.write("\n")
  except Exception as e:
    print(str(e))
    if len(alldata) == 0:
      print("no cache.")
    else:
      print("internal error while loading cache.")
      quit()

  print('List of invalid data:')
  print(invaliddata)
  print('done with cache, press enter...')
  input()
  # load builtins
  try:
    with open(os.path.join(sys.path[0], "hardcoded.json")) as f:
      builtins = json.load(f)
  except:
    print("no builtins.")

  # load blacklist
  try:
    with open(os.path.join(sys.path[0], "black_list.csv")) as f:
      blacklist_df = pd.read_csv(f, sep=';')
      # blacklist = blacklist_df.iloc[:, 0].array
      blacklist = dict(zip(blacklist_df.iloc[:, 0].array, blacklist_df.iloc[:, 1].array))
  except:
    print("no blacklist.")

  for i in sys.argv[1:]:
    base = re.search(r'(.*)\.pdf',i)
    if base: # otherwise we don't have a pdf file!

      base = base.group(1)
      filename = re.search(r'.*\/([^\/]*\.pdf)',i).group(1)

      # skip unknown PDF files
      res = {}
      if files_df is not False:
        res = files_df.loc[files_df['filename'] == filename]
        if res.empty:
          print("SKIP ", filename, " (unknown from the file DBs)")
          continue
      
      # skip already processed files
      if filename in allfilenames:
        print("SKIP ", filename, " (already in cache)")
        continue

      # convert pdf to a collection of html and image files
      # os.system("pdftotext -enc UTF-8 \"{}\" > /dev/null".format(i))
      os.system("pdftohtml -enc UTF-8 \"{}\" > /dev/null".format(i))
      os.system("pdftotext -enc UTF-8 \"{}\" > /dev/null".format(i))

      # the interesting html file has the 's' suffix
      htmlfile = "{}s.html".format(base)
      textfile = "{}.txt".format(base)

      # cleanup non utf8 characters (this happens sometimes despite the -enc UTF-8 option)
      os.system("iconv -f utf-8 -t utf-8 -c '" + htmlfile + "' > tmp.txt ; mv tmp.txt '" + htmlfile + "'")
      os.system("iconv -f utf-8 -t utf-8 -c '" + textfile + "' > tmp.txt ; mv tmp.txt '" + textfile + "'")
      
      print("analyse file \"{}\"".format(htmlfile))
      itemdata = pdf_extract.pdf_extract(htmlfile)

      if not itemdata:
        print("skip invalid file \"{}\"".format(i))
        itemdata['model'] = 'invalid'
        itemdata['filename'] = filename
        itemdata['md5'] = compute_md5(i)
      else:

        if (files_df is not False) and (not res.empty):
          itemdata['model2']    = res['fullname'].array[0]
          itemdata['category']  = res['category'].array[0]
          itemdata['source']    = res['url'].array[0]

        if not 'source' in itemdata:
          itemdata['source'] = filename

        itemdata['filename'] = filename
        itemdata['md5'] = compute_md5(i)

        # ok, looks like we successfully extracted some bits of information,
        # now let's analyze the image file to extract percentages from piecharts...
        chartdata = {}
        skip_piechat = False
        if itemdata['brand']=='HP':
          with open(textfile) as f:
            if 'Manufacturing Breakout' in f.read():
              skip_piechat = True
              chartdata = {}
              chartdata = pdf_extract.hp_addons(textfile,chartdata)
              itemdata = append_chartdata(itemdata,chartdata)

        if not skip_piechat and not filename in blacklist and itemdata['brand']!='HPE' and itemdata['brand']!='Apple':
          # try all png/jpg files...
          for chartfile in glob.iglob("{}-1_?.*".format(base)):
            # skip some dirty piecharts
            md5 = subprocess.check_output("md5 -q \"{}\"".format(chartfile), shell=True).strip()
            if md5 == b'aa85c782a0b4b9b1db6c99c91a47d0c0':
              continue
            
            print("   \"{}\" ...".format(chartfile))
            if 'brand' in itemdata:
              tmp = unpie.analyze(chartfile, ocrprofile=itemdata['brand'])
            else:
              tmp = unpie.analyze(chartfile)
            if tmp:
              # FIXME, maybe we could stop here,
              # TODO, let analyze return a confidence score...
              if len(tmp.keys()) > len(chartdata.keys()):
                chartdata = tmp
        
          if not chartdata:
            print("   -> cannot extract any pie charts data from extracted images.")
            print("   -> try with fullpage rendering...")
            os.system("pdftoppm -png -l 1 -r 220 \"{}\" tmp > /dev/null".format(i))
            fullpage="{}_fullpage1.png".format(base)
            cropped=fullpage
            os.system("mv tmp-1.png {}".format(fullpage))
            # os.system("convert tmp-1.ppm tmp-1.png")
            if 'brand' in itemdata:
              if itemdata['brand'] == "HP" or itemdata['brand'] == "DELL":
                cropped="{}_cropped.png".format(base)
                os.system("convert {} -crop 100%x50% tmp-2.png".format(fullpage))
                os.system("mv tmp-2-1.png {}".format(cropped))
              elif itemdata['brand'] == "Lenovo":
                cropped="{}_cropped.png".format(base)
                os.system("convert {} -crop 1120x850\!+750+650 +repage {}".format(fullpage,cropped))
            tmp = unpie.analyze(cropped, ocrprofile=itemdata['brand'])
            if tmp:
              chartdata = tmp

          if chartdata:
            builtin = builtins['chart percentages'].get(filename)
            if builtin:
              for k,v in builtin.items():
                chartdata[k] = v
              if 'use' in builtin and 'transp' in builtin and 'EOL' in builtin and not 'prod' in builtin:
                chartdata['prod'] = 100 - builtin['use'] - builtin['transp'] - builtin['EOL']
              chartdata['profile/main'] = chartdata['profile/main'] + '+builtin'
              chartdata['confidence1'] = 1
              chartdata['confidence2'] = 1
            itemdata = append_chartdata(itemdata,chartdata)
          else:
            print("WARNING failed to extract any piechart data :(")
        else:
          # this file is broken,
          msg=""
          builtin = builtins['chart percentages'].get(filename)
          if builtin:
            chartdata = {}
            for k,v in builtin.items():
              chartdata[k] = v
            msg = 'builtin '
            itemdata = append_chartdata(itemdata,chartdata)
          # record the reason in the 'profile/main' attribute
          if skip_piechat:
            msg += 'new HP'
          elif filename in blacklist:
            msg += blacklist[filename]
          elif not builtin:
            msg = 'skip '+itemdata['brand']
          itemdata['profile/main'] = msg
        
        builtin = builtins['general'].get(filename)
        if builtin:
          for k,v in builtin.items():
            itemdata[k] = v

        itemdata['extract_date'] = now.strftime("%Y/%m/%d")
        
        print(itemdata,"\n")

        alldata.append(itemdata)
        
      with open("cache/extract_all_cache.json", "a") as f:
        f.write(json.dumps(itemdata))
        f.write("\n")
      
      allfilenames.append(filename)

      
  df = pd.DataFrame(alldata)

  main_percentages = ['use_p', 'prod_p', 'transp_p', 'EOL_p']
  detail_percentages = []
  for k in df.columns:
    if k.endswith('_p') and not k in main_percentages:
      detail_percentages.append(k)


  alldata_csv = df.to_csv()
  # print("\n\n")
  # print(alldata_csv)
  
  output = "cache/all_" + now.strftime("%Y%m%d_%H_%M_%S") + ".csv"
  with open(output,"w+") as f:
    f.write(alldata_csv)

  output = "ecodiag_footprint_database.csv"
  with open(output,"w+") as f:
    f.write(alldata_csv)



  # found_piechart=0
  # for img in "$base"*1_?.png; do
  #   echo "Try \"$img\" ..."
  #   if python3.7 reverse_piechart.py -i "$img";  then
  #     found_piechart=1
  #   fi
  # done
  # if [ found_piechart -eq 1 ]; then
  #   echo "ERROR no pie chart found"
  #   echo ""
  # fi
# done




