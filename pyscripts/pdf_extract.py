# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

import cv2
# import numpy as np
import argparse
# import json
import re
import os
from word2number import w2n
import calendar
import pytesseract
import math

def fail(attr):
  print("WARNING missed ",attr)

def pdf_extract(filename):
  data = {}

  with open(filename) as f:
    txt = f.read()

    txt = re.sub(r'&#160;',' ',txt)
    txt = re.sub(r' ','',txt)
    txt = re.sub(r'(\<br\/\>|\<b\>|\<\/b\>|\<i\>|\<\/i\>)','',txt)
    txt = re.sub(r'(\<span[^\>]*\>|\<\/span\>)','',txt)
    txt = re.sub(r'&#34;','"',txt)

    if (not "CO2" in txt) and (not "kg CO e" in txt):
      return {}

    def parse_attr(r,attr,quiet=False, flags=0):
      res = re.search(r,txt,flags=flags)
      if res:
        data[attr] = res.group(1).strip()
        return res
      elif not quiet:
        fail(attr)
      return False

    brand = False
    if re.search(r' HPE[^A-Za-z]',txt):
      brand = 'HPE'
    elif re.search(r' HP ',txt):
      brand = 'HP'
    elif re.search(r'Dell\s',txt):
      brand = 'DELL'
    elif re.search(r' Apple ',txt):
      brand = 'Apple'
    elif re.search(r'Lenovo',txt):
      brand = 'Lenovo'
      
    if brand:
      data['brand'] = brand
    else:
      print("ERROR cannot identify brand")

    is_server = False

    if brand=='HP':

      # Product carbon footprint 
      # HP ElitePOS G1 14 inch Touch AiO 

      # Product carbon footprint 
      # HP Chromebook Enterprise x360 14E 
      # G1 
      # Estimated impact

      # Product carbon footprint 
      # Sprout Pro by HP G2 
      # Estimated impact 

      # carbon footprint 
      # HP Desktop Pro G2 MT 
      # Estimated impact 

      # Product Carbon Footprint Report
      # HP E24mv G4 Collaboration Monitor
      # Estimated impact

      if parse_attr(r'(?i)carbon footprint\s*(?:report)?\n\s*((.*\n){1,2})Estimated','model', flags=re.MULTILINE):
        # cleanup
        data['model'] = data['model'].replace('\n','')
        data['model'] = re.sub(r'(^HP | [Mm]onitor| [Dd]isplay| [Nn]otebook PC)', '', data['model'].strip())
        
      # Copyright 2019 HP

      # Copyright 2017 Hewlett-Packard Development Company
      parse_attr(r'Copyright\s+(2[0-9]+)\s+(HP|Hewlett)','year')

      has_mean_of = re.search(r'mean of',txt)
      if has_mean_of:
        # mean of 726 kg of CO2-e and 
        parse_attr(r'mean of\s*([0-9]+\.?[0-9]*)\s*kg','global_mean')

        # standard deviation of 168 kg of
        parse_attr(r'deviation of\s*([0-9]+\.?[0-9]*)\s*kg','global_stdev')
        
        # Estimated impact 
        # 
        # 396 - 
        # 1620† 
        # kgCO2e †All estimates of carbon 

        # Estimated impact 
        #
        #265 - 645† 

        # Estimated impact 
        # 
        # 155-540† 
        # kgCO2e 

        # Estimated impact 
        # 
        # 205 - † 360 
        # kgCO2e

        # Estimated impact 
        # 240 - 765† 
        #parse_attr(r'Estimated impact.*\n.*\n\s*([0-9]+\.?[0-9]*)(\s|\-)','global5p')
        parse_attr(r'Estimated impact.*\n[^0-9]*([0-9]+\.?[0-9]*)(\s|\-)','global5p')
        parse_attr(r'Estimated impact.*[^\-]*\-[^0-9]*([0-9]+\.?[0-9]*)','global95p')

      else:
        # Estimated impact 
        # 290  
        # kgCO2e
        # +/- 40 kgCO2 e (average with  one standard deviation) 

        # Estimated impact
        # 286
        # kg CO2 eq.

        # Estimated impact
        # 420
        # kg CO eq
        parse_attr(r'(?i)Estimated impact.*\n\s*([0-9]+\.?[0-9]*)\s*\n\s*kg\s*CO','global_mean')

        parse_attr(r'\+\/\-\s*([0-9]+\.?[0-9]*)\s*kg\s*CO2','global_stdev')

    elif brand=='DELL':

      # Dell C2422HE Monitor (without stand) 
      # ) 
      # Report produced Feb 2021 
      if not parse_attr(r'Dell\s*(.*[^\s])\s*\n(\)\s\n)?\s*Report','model', quiet=True):
        parse_attr(r'((?:PowerEdge|PowerStore|Precision|OptiPlex|Latitude).*[^\s])\s*\n\s*Report','model')
      
      # Report produced December, 2018
      # Report produced April 2020
      parse_attr(r'Report produced ([A-Za-z]+)\b','month')
      parse_attr(r'Report produced.*,? (2[0-9]{3})','year')

      # footprint: 
      # 510 kgCO2e +/- 93 kgCO2e 
      parse_attr(r'footprint:.*\n\s*([0-9]+\.?[0-9]*)\s*kgCO2','global_mean')
      parse_attr(r'\+\/\-\s*([0-9]+\.?[0-9]*)\s*kgCO2','global_stdev')

      is_server = 'model' in data and 'PowerEdge' in data['model']

      # Assumptions for calculating product carbon footprint: 
      # Assembly   
      # Product Weight   44 kg  
      # Server Type   Rack   
      # EU  
      # Location   
      # Energy Demand   
      # Product Lifetime   4 years  
      # Use Location   EU  
      # 3012.126 kWh  
      # (Yearly TEC)   
      # x2 300GB 2.5”  
      # HDD/SSD   
      # HDD  
      # DRAM Capacity   64BG  
      # CPU Quantity    2  
      # Quantity   
      #   x4 2TB 2.5” HDD  
      #
      # 1 of these products…   

      # -> since this is a mess, the idea consists in remove all we know, and the remaining lines are the storage description
      block = re.search(r'Assumptions for calculating product carbon footprint.*\n((.*\n)+)\s*1 of these products', txt, re.MULTILINE)
      if is_server and block:
        block = block.group(1)
        # print(block,'---')
        # now we remove all known lines
        block = re.sub(r'(?m)^.*(Assembly|Weight|Server|EU|Location|Energy|Lifetime|kWh|TEC|DRAM|Quantity).*\n?','',block)
        block = re.sub(r'HDD\/SSD','',block)
        # print(block,'---')
        # remove empty lines and \n
        block = re.sub(r'(?m)\n\s*\n?','',block).strip()
        # remove duplicated spaces
        block = re.sub(r'\s\s+',' ',block)
        block = re.sub(r'BG','GB',block) # fix typos
        if block:
          data['conf_storage'] = block
        elif is_server:
          fail('conf_storage')
      elif is_server:
        fail('conf_storage')

        # res = re.search(r'HDD\/SSD.*\n\s*(.*)\s*\n.*\n.*\n\s*Quantity\s*\n\s*(SSD|HDD)?\s*', txt)
        # if res:
        #   data['conf_storage'] = res.group(1).strip()
        #   if len(res.groups()) >= 3:
        #     data['conf_storage'] += ' ' + res.group(2).strip()
        # elif is_server:
        #   fail('conf_storage')

    elif brand=='HPE':

      # FOOTPRINTHPE ProLiant DL360 Gen10 server
      # At
      parse_attr(r'FOOTPRINTHPE\s*(.*)\s*\nAt','model')

      # ESTIMATED PRODUCT CARBON FOOTPRINT:* 6270 kg CO e
      parse_attr(r'FOOTPRINT:[^0-9]*([0-9]+\.?[0-9]*)\s*kg CO','global_mean')

      # standard deviation of 1430 kg CO e
      parse_attr(r'standard deviation of\s*([0-9]+\.?[0-9]*)\s*kg CO','global_stdev')

      is_server = True

      # SSD quantity (storage)
      # 4
      if parse_attr(r'(?i)SSD quantity[^0-9]*\s*([0-9]+)','conf_storage', quiet=not is_server):
        data['conf_storage'] += ' SSD'

      # Use (87.4%) 
      if parse_attr(r'(?i)Use\s*\(([0-9]+\.?[0-9]*)\%\)','use_p'):
        data['use_p'] = float(data['use_p'])

      # Supply chain (11.9%) 
      # parse_attr(r'(?i)Supply chain\s*\(([0-9]+\.?[0-9]*)\%\)','supplychain_p')

      def parse_detail(labelIn,labelOut,quiet=False):
        if parse_attr(r'(?i)'+labelIn+'\s*([0-9]+\.?[0-9]*)\s*kg CO',labelOut, quiet=quiet):
          data[labelOut] = float(data[labelOut])
          data[labelOut+'_p'] = round(data[labelOut] / float(data['global_mean']) * 100, 1)
          return True
        return False

      # SSD 65.2 kg CO e
      parse_detail('SSD','SSD')

      # Mainboard 365 kg CO e
      if not parse_detail('Mainboard','board',quiet=True):
        # Mainboard
        # SSD 113 kg CO e
        # 2
        # 373 kg CO e
        if parse_attr(r'(?i)Mainboard\s*SSD\s*[0-9]+\.?[0-9]*\s*kg CO e\s*.\s*([0-9]+\.?[0-9]*)\s*kg CO','board'):
          data['board'] = float(data['board'])
          data['board'+'_p'] = round(data['board'] / float(data['global_mean']) * 100, 1)

      # Daughterboard(s) 224 kg CO e
      parse_detail('Daughterboard[^0-9]*','daughterboard')

      # Enclosure, Fan(s), PSU 90.3 kg CO e
      parse_detail('Enclosure, Fan\(s\), PSU','box_PSU')

      # Transport 31.1 kg CO e
      parse_detail('Transport','transp')

      # End of life 8.37 kg CO e 
      parse_detail('End of life','EOL')

      # Assembly 4.7 kg CO e
      parse_detail('Assembly','assembly')

      if 'EOL_p' in data and 'transp_p' and 'use_p' in data:
        data['prod_p'] = 100 - data['EOL_p'] - data['transp_p'] - data['use_p']

      # a50002430ENW, October 2021, Rev. 1
      res=re.search(r'(?i)('+('|'.join(calendar.month_name[1:]))+').*(2[0-9]{3})', txt)
      if res:
        data['year'] = res.group(2)
        data['month'] = res.group(1)
        data['model'] += ' '+data['year']


  if brand=='HP' or brand == 'DELL' or brand == 'HPE':

    # Product Weight 
    # 7.335 kg 
    # Product Weight   20.2 kg
    # Product weight 
    # 1113  grams
    # Product weight 
    # 5. 02 kg
    # Product Weight 
    # 1.11 g
    # Product weight (kg)
    # 6.5
    pres = parse_attr(r'(?i)weight\s*(?:.kg.\s*)?\n?\s*([0-9]+(\.|,|\. )?[0-9]*)\s*(kg|grams|g|\n)','weight', quiet=re.search(r'\[enter value\]\s+kg', txt))
    if pres:
      data['weight'] = data['weight'].replace(',','.')
      data['weight'] = data['weight'].replace('. ','.')
      if pres.group(3) == 'grams' or pres.group(3) == 'g':
        val = float(data['weight'])
        if val>10:
          data['weight'] = val/1000
        else:
          print("WARNING read", val, "g, but those should likely be read as kg. This value is left unchanged.")

    
    # Screen Size 
    # 23.8” 

    # Screen size 
    # 27  inches 

    parse_attr(r'(?i)screen size.*\n([0-9]+\.?[0-9]*)\s*(”|"|inches)','size', quiet=True)

    # Product Lifetime 
    # 6 years 

    # Lifetime of product 
    # 15 years 
    # Product Lifetime   4 years 

    # Product lifetime
    # 4 years

    # Lifetime of product (years)
    # 5
    parse_attr(r'(?:)ifetime.*\n?\s+([0-9]+)(?: years|\n)?','lifetime')

    # Use location 
    # Worldwide 
    # Use Location   EU 

    # Use location
    # North America
    parse_attr(r'(?i)Use location\s*\n?\s*([A-Za-z ]+)\s*','useloc')

    # Assembly location
    # EU

    # Final manufacturing location
    # China
    parse_attr(r'(?i)(?:Assembly|Final manufacturing) location\s*\n?\s*([A-Za-z ]+)\s*','assemblyloc')
    
    # 39.5655 kWh 
    # (Yearly TEC) 

    # Use energy demand (Yearly TEC) 
    # 76.74  kWh 

    # Use energy demand (Yearly TEC) <br/>
    # 18.29] kWh 

    # Use energy demand (kWh/year) 
    # 15.9

    # Use energy demand (Yearly TEC) 
    # 70.91
    if re.search(r'Use energy demand', txt):
      parse_attr(r'(?i)Use energy demand.*\n([0-9]+\.?[0-9]*)\]?','kWh')
    else:
      parse_attr(r'([0-9]+\.?[0-9]*)\]?\s*kWh','kWh', quiet=re.search(r'\[enter value\]\s+kWh', txt))

    # Server type
    # Rack
    # Server Type  Rack
    parse_attr(r'(?i)Server Type\s*([A-Za-z]+)','conf_server_type', quiet=not is_server)
    # if parse_attr(r'Server Type\s*([A-Za-z]+)','conf_server_type', quiet=not is_server):
    #   val = data['conf_server_type']
    #   if val == 'R':
    #     data['conf_server_type'] = 'Rack'
    #   elif val == 'B':
    #     data['conf_server_type'] = 'Blade'
    #   elif val == 'T':
    #     data['conf_server_type'] = 'Tower'

    # CPU quantity (mainboard)
    # 1
    # CPU Quantity   2  
    parse_attr(r'(?i)CPU Quantity[^0-9]*\s*([0-9]+)','conf_nb_cpus', quiet=not is_server)

    # DRAM capacity (mainboard)
    # 48 GB
    # DRAM Capacity   16GB
    parse_attr(r'(?i)DRAM capacity[^0-9]*\s*([0-9]+\.?[0-9]*)\s*(GB|BG)','conf_DRAM', quiet=not is_server)

    # PAIA model, version  1.2.8,  2020, copyright  by  the  ICT

    # (PAIA) model, Display Version 
    # 1_11.22, copyright by the ICT

    # (PAIA) model, Display Version 
    # qe
    # 1_11.22, copyright by the ICT

    # Product Attribute to Impact Algorithm <br/>
    # O
    # (PAIA) model, Display Version 
    #  C
    # 800
    # 1_11.22, copyright by the ICT 
    # kg(t 
    # Benchmarking collaboration, which 
    has_copyright = re.search(r'copyright',txt)
    # parse_attr(r'PAIA.*\n?.*[\s\n]\s*([0-9]+(_|\.)[0-9\.]+)','paia',not bool(has_copyright))
    parse_attr(r'([0-9]+(_|\.)[0-9\.]+)[^0-9].*copyright','paia',quiet=True)

    if not 'paia' in data:
      # try:

      # (PAIA) model, November 2016 
      # Desktop Version, copyright by the ICT

      # (PAIA) model, April 2015 AiO Version,
      parse_attr(r'PAIA.*\, ([A-Za-z]+ [0-9]{4})','paia',not bool(has_copyright))
    if brand=='HP' and not 'paia' in data and re.search(r'HP-specific',txt):
      data['paia'] = 'HP-specific'

  if brand=='Apple':
    # Product Environmental Report 
    # 16-inch MacBook Pro 

    # 15-inch MacBook Pro  with Retina display Environmental Report

    if "Product Environmental Report" in txt:
      parse_attr(r'Product Environmental Report\s*\n\s*(.*)\s*\n','model')
    else:
      parse_attr(r'\s*(.*)\s*Environmental Report','model')

    # 185 kg carbon 
    # emissions
    # 349 kg carbon
    # emissions3
    if parse_attr(r'([0-9]+\.?[0-9]*)\s*kg.*carbon','global_mean',quiet=True):
      print('Apple mode 1')
      #  73%  Production 
      # to the needs of Mac devices. This change decreased the carbon footprint of the main logic board, 
      #   7%  Transport 
      # driving down the overall 13-inch MacBook Pro carbon footprint by 12 percent compared with the 
      # 19%   Use  &lt;1%  End-of-life processing
      parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%\s*Production','prod_p')
      parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%\s*Transport','transp_p')
      parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%\s*Use','use_p')
      parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%\s*End\-of\-life','EOL_p')
    
    # Total greenhouse gas emissions: 320 kg CO2e
    # Total greenhouse gas equivalent: 15 kg CO2e
    elif parse_attr(r'Total greenhouse gas (?:emissions|equivalent):\s*([0-9]+\.?[0-9]*)\s*kg.*CO','global_mean',quiet=True):
      print('Apple mode 2')
      # Recycling, <1%
      # Transport, 5%
      # Production, 75%
      # Customer use, 19%
      parse_attr(r'(?i)Production\,[^0-9\%\n]*([0-9]+\.?[0-9]*)\%','prod_p')
      parse_attr(r'(?i)Transport\,[^0-9\%\n]*([0-9]+\.?[0-9]*)\%','transp_p')
      parse_attr(r'(?i)Use\,[^0-9\%\n]*([0-9]+\.?[0-9]*)\%','use_p')
      parse_attr(r'(?i)Recycling\,[^0-9\%\n]*([0-9]+\.?[0-9]*)\%','EOL_p')
    else:
      txt2 = txt
      isOld=False
      if not 'kg carbon' in txt:
        print("   Apple's PDF is an image, try with fullpage rendering and OCR...")
        res=re.search(r'(.*)s\.html', filename)
        if res:
          basefilename = res.group(1).strip()
          isOld = not 'Product Environmental Report' in txt
          p = 1 if isOld else 2
          os.system("pdftoppm -png -l {} -r 220 \"{}\" \"{}\" > /dev/null".format(p, basefilename+'.pdf', basefilename+'-fp'))
          page=basefilename+'-fp-{}.png'.format(p)
          custom_oem_psm_config = '--psm 12 --oem 1'
          txt = pytesseract.image_to_string(cv2.imread(page,cv2.IMREAD_COLOR), config=custom_oem_psm_config).strip()
          # print(txt)
          if isOld:
            print('  old mode')
            parse_attr(r'(?i)Greenhouse(?:.*\n)*[^0-9]*([0-9]{2,})\s(?:.*\n)*.*kg CO','global_mean')
            parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%[^\%]*Production','prod_p')
            parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%[^\%]*Transport','transp_p')
            parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%[^\%]*Use','use_p')
            parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%[^\%]*Recycling','EOL_p')
          else:
            parse_attr(r'([0-9]+\.?[0-9]*)\s*kg.*carbon','global_mean')
            parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%\s*Production','prod_p')
            parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%\s*Transport','transp_p')
            parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%\s*Use','use_p')
            parse_attr(r'(?i)([0-9]+\.?[0-9]*)\%\s*End\-of\-life','EOL_p')

      txt = txt2

    # Date introduced 
    # November 13, 2019
    # 
    # # Date introduced
    # as it relates to climate change, energy efficiency, material efficiency, and 
    # May 19, 2015
    #
    # March 19, 2019

    res=re.search(r'(?i)('+('|'.join(calendar.month_name[1:]))+').*\, (2[0-9]{3})', txt)
    if res:
      data['year'] = res.group(2)
      data['month'] = res.group(1)
      data['model'] += ' '+data['year']

    # parse_attr(r'Date introduced\s*\n?.*\n?\s*(January|February|March|April|May|June|July|August|September|October|November|December)\s','month')
    # parse_attr(r'Date introduced\s*\n?.*\n?\s*(?:January|February|March|April|May|June|July|August|September|October|November|December).*\, (2[0-9]+)','year')

    parse_attr(r'(?i)([0-9]+)-inch','size')

    # assumes a four-year
    parse_attr(r'assumes a ([a-z]+)\-year\s','lifetime')
    try:
      num = w2n.word_to_num(data['lifetime'])
      data['lifetime'] = num
    except Exception:
      print("WARNING fail to extract lifetime")

    data['useloc'] = "WW"

  if brand=='Lenovo':

    # Commercial  Name    
    # ThinkStation  P310          
    # Model  Number  
    # 30AS,30AT,   30AY,30B0  
    #    
    # Issue  Date    
    # December.15th,  2015 

    # Commercial Name 
    # ThinkStation P340 Tower 
    # Model Number 
    # 30DH, 30DJ, 30DM 
    # Issue Date 
    # 2020/5/15 

    parse_attr(r'Commercial\s+Name\s*\n\s*(.*)\s*\n','model')
    parse_attr(r'Issue\s+Date\s*\n.*(2[0-9]{3})','year')
    if not parse_attr(r'Issue\s+Date\s*\n.*('+('|'.join(calendar.month_name[1:]))+')','month',quiet=True):
      if parse_attr(r'Issue\s+Date\s*\n\s*2[0-9]{3}(?:\/|\-|\.)([0-9]{1,2})[^0-9]','month'):
        try:
          data['month'] = calendar.month_name[int(data['month'])]
        except Exception:
          print("WARNING: failed to convert", data['month'], " to month.")
          pass

    # a mean of 351 kg of CO2e and standard deviation of 104 kg of CO2e
    parse_attr(r'mean\s+of\s+([0-9]+\.?[0-9]*)\s+kg\s+of\s+CO2','global_mean')
    parse_attr(r'deviation\s+of\s+([0-9]+\.?[0-9]*)\s+kg\s+of\s+CO2','global_stdev')


    # 1534  kg  of  CO2e
    # 532 kg of CO2e
    parse_attr(r'([0-9]+\.?[0-9]*)\s+kg\s+of\s+CO2','global95p')

    # Product Attribute to Impact Algorithm model, Version 2, Date: February 2015 
    # Product Attribute to Impact Algorithm model, Version 1/20/2018, Date: 1/20/2018
    parse_attr(r'Product Attribute to Impact Algorithm model\, Version .* Date\: ([a-zA-Z]+ [0-9]+|[0-9]+\/[0-9]+\/[0-9]+)\s','paia')

    # Product Weight 
    # kg 
    # Input 
    # 13

    # Product Weight 
    # kg 
    # 4.00 
    parse_attr(r'(?i)Product Weight\s*\n.*kg.*\n(?:.*Input.*\n)?\s*([0-9]+\.?[0-9]*)','weight')

    # Product Lifetime 
    # years 
    # Input 
    # 3
    parse_attr(r'Product Lifetime\s*\n.*years.*\n(?:.*Input.*\n)?\s*([0-9]+\.?[0-9]*)','lifetime')

    # Use Location 
    # no unit 
    # US

    # Use Location 
    # no unit  Default
    parse_attr(r'Use Location\s*\n.*no unit\s*(?:\n|\s)\s*([a-zA-Z]+)','useloc')

    # Screen Size 
    # inches
    # 23.6
    # Screen Size 
    # inches  23.6
    parse_attr(r'(?i)Screen Size\s*\n.*inches(?:.*\n)?\s*([0-9]+\.?[0-9]*)','size', quiet=True)

  return data


def hp_addons(filename, data):
  with open(filename) as f:
    txt = f.read()

    def parse_attr(r,attr,quiet=False, flags=0):
      res = re.search(r,txt,flags=flags)
      if res:
        data[attr] = float(res.group(1).strip())
        return res
      elif not quiet:
        fail(attr)
      return False
    
    parse_attr(r'(?i)Use\n([0-9]+\.?[0-9]*)\%','use')
    parse_attr(r'(?i)End of Life\n([0-9]+\.?[0-9]*)\%','EOL')
    parse_attr(r'(?i)Distribution\n([0-9]+\.?[0-9]*)\%','transp')
    parse_attr(r'(?i)Manufacturing\n([0-9]+\.?[0-9]*)\%','prod')

    def parse_detail(r,attr):
      res = re.search(r,txt)
      if res:
        data[attr] = round(float(res.group(1).strip()) * data['prod'] / 100., 2)
        return res
      return False

    parse_detail(r'(?i)Display\n\n([0-9]+\.?[0-9]*)\%','disp')
    parse_detail(r'(?i)Chassis\n\n([0-9]+\.?[0-9]*)\%','box')
    parse_detail(r'(?i)Mainboard .*\n\n([0-9]+\.?[0-9]*)\%','board')
    parse_detail(r'(?i)Power .*\n\n([0-9]+\.?[0-9]*)\%','power')
    parse_detail(r'(?i)Packaging\n\n([0-9]+\.?[0-9]*)\%','packaging')
    parse_detail(r'(?i)SSD.\n\n([0-9]+\.?[0-9]*)\%','SSD')
    parse_detail(r'(?i)HDD.\n\n([0-9]+\.?[0-9]*)\%','HDD')
    parse_detail(r'(?i)Batter.*\n\n([0-9]+\.?[0-9]*)\%','battery')
    parse_detail(r'(?i)Other.*\n\n([0-9]+\.?[0-9]*)\%','others')
  return data

if __name__ == "__main__":

  ap = argparse.ArgumentParser(description="This routine extracts information from the .html file generated from pdftohtml.")

  ap.add_argument("-i", "--input", required=True, help="input html file")
  
  args = vars(ap.parse_args())

  res = pdf_extract(args['input'])

  print(res)


