#/bin/bash

ts=`date '+%Y%m%d_%H_%M'`

function bck() {
  base=`echo "$1" | sed -n -E 's/(.*)\.[^\.]*/\1/p'`
  ext=`echo "$1" | sed -n -E 's/.*\.([^\.]*)/\1/p'`
  base1=`echo "$base" | sed -n -E 's/(.*\/)?([^\/]*)/\2/p'`
  src="$1"
  dst="bck/$base1"_"$ts.$ext"
  cp "$src" "$dst"
  echo "Backup $src in $dst"
}

mkdir -p bck

if python3.8 pyscripts/extract_all.py cache/*/DELL/*.pdf cache/*/HP/*.pdf cache/*/Lenovo/*.pdf cache/HPE/*.pdf cache/Apple/*.pdf; then
  echo ""
  echo "Extraction successful."
  echo ""

  bck cache/extract_all_cache.json
  
  bck ecodiag_footprint_database.csv

  echo "--------------------------------------------------"
  echo ""
  echo "Merge with Boavizta..."
  python3.8 pyscripts/merge_with_boavizta.py -i ecodiag_footprint_database.csv -b boavizta-data-us.csv -o ecodiag_boavizta_footprint_database.csv

  bck ecodiag_boavizta_footprint_database.csv
else
  echo "Error during PDF extraction, skip merge with Boavizta"

  bck cache/extract_all_cache.json
fi

