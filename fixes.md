# Fixes

The manufacturer's PCF sheets exhibit numerous typos and inconsistencies.


## Manual fixes

Some of them are impossible to recover in an automatic manner and are thus manually corrected by filling up the "general" field of the [hardcoded.json](pyscript/hardcoded.json) file.
Some of those issues include:

* Wrong unit for the weight (e.g., grams or lbs written as kg)
* Decimal separator is missing or put at the wrong place for the weight or kWh attributes.
* The 'global_mean' and 'global_stdev' is written at different locations of the document but with different values.

This file also contains a "chart percentages" field to manually workaround charts that are difficult or impossible to automatically recover.
Some of those issues include:

* Very poor image quality (low resolution, extreme jpeg compression)
* Labels are very close to each others and difficult to separate.
* Wrong labels, e.g.: 'Manufacturing' is written twice but one of the twos should be read as 'Use', 'Manufacturing' and 'Use' are inverted.
* Incomplete legend.


## HP fixes based on 'kWh/y' versus 'Use'

Several HP's PCF sheets exhibit clear incoherence between the exposed 'kWh/y' electricity consumption and the emissions of the use phase recovered from the 'global_mean' and 'use' percentage.
In theory we should have:
`global_mean * use% ≥ kWh/y * elec_factor * lifetime`.
The left hand side might be higher than the right hand side if the use phase includes maintenance operations, cooling, etc. Except for servers, an equality is expected.
The `elec_factor` is not explicitly provided by the manufacturer's sheets, but it can be estimated from the other values:
`elec_factor = (global_mean * use%) / (kWh/y * lifetime)`.
By analyzing this recovered `elec_factor` for all HP's sheets, we can see that HP is using three different values: 0.686, 0.525, and 0.62 kgCO2e/kWh.
Therefore, sheets implying an `elec_factor` way below 0.525 or higher than 0.7 (for non server devices) are highly suspicious.

By analyzing those suspicious sheets, we found two recurrent patterns: an invalid `global_mean` or an invalid `lifetime`.

### HP fix 1: invalid `lifetime`

Excluding servers, many sheets imply an abnormally high `elec_factor`, but one of the three aforementioned typical electricity factor can be precisely recovered if the `lifetime` value is changed for a smaller integer number of years.
In this case, the attribute `lifetime` is changed accordingly and the word `lifetime` is added to the attribute `extrapolated`.
However, since the presented `lifetime` seems to be reasonable and consistent with other similar models, a more correct fix would probably be to recompute the use phase and update the `global_mean` (and all attributes depending on this one) accordingly.

### HP fix 2: invalid `global_mean`

For many other sheets, e.g., [this one](https://h20195.www2.hp.com/v2/GetDocument.aspx?docname=c07525009), we can observe a clear inconsistency between the values obtained by multiplying `global_mean` by the pie-chart percentages, and the values we can read on the bar-plot of the second page. For instance, for the `use` phase, we can read `~52-54 kg CO2e`, whereas `global_mean * use%` yields `275 * 16.2% = 44.5 kg CO2e`.
The first value yields a consistent `elec_factor` value of 0.525 kg CO2e/kWh, whereas the second yields a rather small, and infrequently used value of `0.42`.
Moreover, the percentages recovered from the second bar-plot matches the one of the pie-chart.
Those observations strongly suggest that `global_mean` is incorrect. Otherwise this would mean that all other informations (the pie-chart, the bar-plot, and the kWh/y) would all be incorrect!

Those observations can be made on all sheets exhibiting an abnormally low `elec_factor` that we checked.

**Fixing rule**: if the apparent `elec_factor` is abnormally low, then `global_mean`, `global_stdev`, `global_5p` and `global_95p` are scaled by a factor `s = (kWh * lifetime * 0.525) / (global_mean * use%)`.
In theory, `global_stdev`, `global_5p` and `global_95p` should be corrected by slightly different factors, but the error introduced by this approximation is rather low compared to the intrinsic error.
The word `global_mean` is added to the attribute `extrapolated`.


