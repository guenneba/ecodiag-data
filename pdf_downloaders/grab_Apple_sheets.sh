#!/bin/bash

# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

# INSTRUCTIONS
# 1) go there : 
#     https://www.apple.com/environment/

bashver=`echo $BASH_VERSION | sed -n -E 's/([0-9]+)\..*/\1/p'`
# echo $bashver
if [ $bashver -lt 4 ]; then
  echo "Error: need bash version 4 or higher"
  exit 1
fi

script_path=$(dirname "$0")
workdir="cache/Apple"
logfile="cache/apple_files.csv"

mkdir -p $workdir

listfile=$workdir/environment.html
if [ -f "$listfile" ]; then
  echo "Skip download of the html listing file."
  echo "Remove \"$listfile\" to force its update."
else
  wget https://www.apple.com/environment
  mkdir -p $workdir
  mv environment $workdir/environment.html
fi



content=`egrep 'products.*View.*PDF' $listfile | sed -E 's/\&amp;nbsp;/ /g'`

# echo $content

echo \"filename\",\"category\",\"fullname\",\"url\" > $logfile

baseurl="https://www.apple.com"

while IFS= read -r line; do
  
  # echo $line
  
  # <a href="/environment/pdf/products/iphone/iPhone_13_Pro_Max_PER_Sept2021.pdf" class="icon-wrapper report-link "  target="_blank" rel="noopener" aria-describedby="" data-analytics-title="iphone 13 pro max - view pdf" aria-label="View (PDF) for the iPhone 13 Pro Max"><span class="icon-copy">View (PDF)</span></a>		</p>
  # ... data-analytics-title="ipod touch 32gb/64gb - view 2014 pdf" ..
  pdflink="$baseurl"`echo "$line" | sed -n -E 's/.*href\=\"([^\"]*pdf).*/\1/p'`
  fullname=`echo "$line" | sed -n -E 's/.*data\-analytics\-title\=\"([^\"]*) \-.*/\1/p'`
  year=`echo "$line" | sed -n -E 's/.*view ([0-9]{4}) pdf.*/\1/p'`

  if [ -n "$fullname" ]; then
    category=""
    if echo $fullname | egrep -q '(?i)iPhone'; then
      category="Smartphone"
    elif echo $fullname | egrep -q '(?i)book'; then
      category="Laptop"
    elif echo $fullname | egrep -q '(?i)pad'; then
      category="Pad"
    elif echo $fullname | egrep -q '(?i)imac'; then
      category="AllInOne"
    elif echo $fullname | egrep -q '(?i)pro'; then
      category="Desktop"
    elif echo $fullname | egrep -q '(?i)mini'; then
      category="Desktop"
    else
      echo "No category for $fullname"
    fi

    "$script_path/"dl_and_append.sh "$pdflink" "$workdir" "$fullname $year" "$category" "$logfile"

  else
    echo "BAD" $line
  fi

done <<< "$content"