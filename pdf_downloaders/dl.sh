#!/bin/bash

url=$1
filename=$2
targetdir=$3

wget "$url"
mkdir -p "$targetdir"
mv "$filename" "$targetdir/"
