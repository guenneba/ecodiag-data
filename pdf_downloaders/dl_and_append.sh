#!/bin/bash

script_path=$(dirname "$0")

url=$1
targetdir=$2
model=$3
category=$4
logfile=$5

filename=`echo $url | sed -n -E 's/.*\/([^\/]*\.pdf).*/\1/p'`

echo \"$filename\",\"$category\",\"$model\",\"$url\" >> $logfile

targetfile="$targetdir/$filename"
# echo "$targetfile   ?"
if [ -f $targetfile ] ; then
    echo $targetfile  OK
else
    "$script_path/"dl.sh "$url" "$filename" "$targetdir"
    sleep 5s
fi
