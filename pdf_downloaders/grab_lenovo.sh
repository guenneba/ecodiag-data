#!/bin/bash

# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

# PROCEDURE:
# goto: https://www.lenovo.com/us/en/compliance/eco-declaration
# open all tabs to load all datas
# get actual html code by entering in the JS console:
#     new XMLSerializer().serializeToString(document.doctype) + document.getElementsByTagName('html')[0].outerHTML
# copy the single-line string into cache/Lenovo/eco-declaration.html file and run this script!

# list=`sed -n -E 's/.*href\=\"(.*pdf).*blank\"\>(.*)\<\/a.*/\1  \2/p' $1`

bashver=`echo $BASH_VERSION | sed -n -E 's/([0-9]+)\..*/\1/p'`
# echo $bashver
if [ $bashver -lt 4 ]; then
  echo "Error: need bash version 4 or higher"
  exit 1
fi

baseurl=""

script_path=$(dirname "$0")
workdir="cache/Lenovo"
logfile="cache/lenovo_files.csv"

listfile="$workdir/"eco-declaration.html
if [ -f "$listfile" ]; then
  echo "Using existing file $listfile"
  echo "Follow the folling procedure to update it."
else
  echo "Missing file $listfile"
  # wget https://www.lenovo.com/us/en/compliance/eco-declaration
  # mkdir -p "$workdir"
  # mv eco-declaration "$listfile"
fi

echo "PROCEDURE:"
echo "goto: https://www.lenovo.com/us/en/compliance/eco-declaration"
echo "get actual html code by entering in the JS console:"
echo '     new XMLSerializer().serializeToString(document.doctype) + document.getElementsByTagName("html")[0].outerHTML'
echo "copy the single-line string into $listfile file and run this script!"

if [ ! -f "$listfile" ]; then
  exit
fi

# split lines on div blocks
# remove \n, \t and double spaces
# keeps lines having class="pcf"
content=`sed -E 's/\<\/div\>/\<\/div\>\n/g' $listfile | sed -E 's/\<div\>/\n\<div\>/g' | sed -E 's/(\\\n|\\\t)//g' | sed -E 's/&nbsp;/ /g' | sed -E 's/  +/ /g' | egrep 'class="pcf".*href|class="tab-pane'`

echo \"filename\",\"category\",\"fullname\",\"url\" > $logfile

declare -A cat2dir=(  ["notebooks"]="cache/Laptop/Lenovo"
                      ["tablets"]="cache/Pad/Lenovo"
                      ["desktops"]="cache/Desktop/Lenovo"
                      ["workstations"]="cache/Desktop/Lenovo"
                      ["monitors"]="cache/Monitor/Lenovo"
                      ["servers"]="cache/Server/Lenovo"
                      ["smart-devices"]="cache/Smart/Lenovo" )

declare -A cat2cat=(  ["notebooks"]="Laptop"
                      ["tablets"]="Pad"
                      ["desktops"]="Desktop"
                      ["workstations"]="Desktop"
                      ["monitors"]="Monitor"
                      ["servers"]="Server"
                      ["smart-devices"]="Smartdevice" )

category=""
while IFS= read -r line; do

  if echo $line | grep -q "tab-pane"; then
    # <div class="tab-pane fade" id="tablets" role="tabpanel" aria-labelledby="tablets-tab">
    tmp=`echo "$line" | sed -n -E 's/.*id\="([^"]*)".*/\1/p'`
    if [ -n "$tmp" ]; then
      echo "Switch to category $tmp"
      category=$tmp
    else
      echo "Fail to extract category from $line"
    fi

  else
    # <div class="pcf"> <img src="........./pdf.ee233046a9b84bd3.gif"> <a class="fbox" target="_blank" href="https://p1-ofp.static.pub/ShareResource/compliance/eco-declaration/pdfs/Batch4/pcf-lenovo-10e-chromebook-tablet.pdf">PCF Lenovo 10e Chromebook</a> </div>
    url=`echo "$line" | sed -n -E 's/.*href\=\"([^\"]*pdf[^\"]*)\".*/\1/p'`
    model=`echo "$line" | sed -n -E 's/.*\<a[^\>]*\>[^a-zA-Z]*PCF[^a-zA-Z]*(.*)\s*\<\/a\>.*/\1/p'`
    model=`echo "$model" | sed -E 's/Lenovo[^a-zA-Z0-9]+//g'`

    if [ -n "$url" ]; then
      if ! echo $url | grep -q "https"; then
        url="https:$url"
      fi

      "$script_path/"dl_and_append.sh "$url" "${cat2dir["$category"]}" "$model" "${cat2cat["$category"]}" "$logfile"
    else
      echo "skip line $line"
    fi

  fi
  
done <<< "$content"
