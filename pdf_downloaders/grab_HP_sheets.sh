#!/bin/bash

# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

# INSTRUCTIONS
# 1) go there : 
#    https://h20195.www2.hp.com/v2/library.aspx?footer=95&filter_doctype=no&filter_country=no&cc=us&lc=en&filter_oid=no&filter_prodtype=rw&prodtype=ij&showproductcompatibility=yes&showregion=yes&showreglangcol=yes&showdescription=yes3doctype-95&sortorder-popular&teasers-off&isRetired-false&isRHParentNode-false&titleCheck-false#doctype-95&sortorder-popular&teasers-off&isRetired-false&isRHParentNode-false&titleCheck-false
# 2) for each product type
#     3) select it and press "Load More" until getting all rows
#     4) in the JS console: console.log(document.documentElement.innerHTML)
#     5) "copy"
#     6) paste it is a new file and save it according the filenames below
# 7) run this script


# list=`sed -n -E 's/.*href\=\"(.*pdf).*blank\"\>(.*)\<\/a.*/\1  \2/p' $1`

# bashver=`echo $BASH_VERSION | sed -n -E 's/([0-9]+)\..*/\1/p'`
# echo $bashver
# if [ $bashver -lt 4 ]; then
#   echo "Error: need bash version 4 or higher"
#   exit 1
# fi

script_path=$(dirname "$0")
workdir="cache/HP"
logfile="cache/hp_files.csv"

mkdir -p $workdir

echo \"filename\",\"category\",\"fullname\",\"url\" > $logfile

data="hp_desktops.html;Desktop;cache/Desktop/HP
      hp_monitors.html;Monitor;cache/Monitor/HP
      hp_notebooks.html;Laptop;cache/Laptop/HP
      hp_workstations.html;Desktop;cache/Desktop/HP
      hp_allinones.html;AllInOne;cache/Desktop/HP"

for d in $data ; do
  filename="$workdir/"`echo "$d" | cut -d ';' -f 1`
  baseurl="https://h20195.www2.hp.com/v2/"
  category=`echo "$d" | cut -d ';' -f 2`
  dest_path=`echo "$d" | cut -d ';' -f 3`

  echo "baseurl=$baseurl"
  echo "filename=$filename"
  echo "category=$category"
  echo "dest_path=$dest_path"
  echo " "

  # all urls are within a single line
  content=`grep 'GetDocument' $filename`
  content=`echo $content | sed -n -E 's/\/tr\>/\/tr\>\n/gp' | grep GetDocument`

  while IFS= read -r line; do

    # echo " $line"
    item_url=`echo $line | sed -n -E 's/.*\<a.*href\="(GetDocument\.aspx[^"]*)" .*/\1/p'`
    item_model=`echo $line | sed -n -E 's/.*\<a.*documentname\="([^"]*)" .*/\1/p'`

    if [ -n "$item_url" ]; then
      item_fullurl="$baseurl$item_url"
      itemid=`echo $item_url | sed -n -E 's/GetDocument\.aspx.docname\=(.*)/\1/p'`
      item_filename="$item_url"
      targetfile="$dest_path/$itemid"".pdf"
      echo \"$itemid".pdf"\",\"$category\",\"$item_model\",\"$item_fullurl\" >> $logfile

      if [ -f $targetfile ] ; then
        echo $targetfile  OK
      else
        wget $item_fullurl
        mkdir -p $dest_path
        mv $item_filename "$targetfile"
        sleep 5s
      fi
      
    fi

  done <<< "$content"

done
