#!/bin/bash

# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.



bashver=`echo $BASH_VERSION | sed -n -E 's/([0-9]+)\..*/\1/p'`
if [ $bashver -lt 4 ]; then
  echo "Error: need bash version 4 or higher"
  exit 1
fi

script_path=$(dirname "$0")
workdir="cache/HPE"
logfile="cache/hpe_files.csv"

listfile=$workdir/sustainable-it.html

if [ -f "$listfile" ]; then
  echo "Skip download of the html listing file."
  echo "Remove \"$listfile\" to force its update."
else
  wget https://www.hpe.com/us/en/living-progress/sustainable-it.html
  mkdir -p $workdir
  mv sustainable-it.html $workdir
fi


content=`sed -E 's/(\<\/[^\>]*\>)/\1\n/g' $listfile | sed -E 's/  +/ /g' | egrep 'href.*PCF'`

echo \"filename\",\"category\",\"fullname\",\"url\" > $logfile

while IFS= read -r line; do

  #...<a href="https://www.hpe.com/us/en/collaterals/collateral.a50005154enw.HPE-product-carbon-footprint-E2-80-93-HPE-ProLiant-MicroServer-Gen10-Plus-data-sheet.html?rpv=cpf&amp;parentPage=/us/en/living-progress/sustainable-it" data-analytics-uaid="1df0095a-3a94-45fd-9bde-89523c57a9ab" data-analytics-assetgated="false" data-analytics-action="resource-click" data-analytics-pub-id="a50005154enw" data-analytics-assetname="HPE product carbon footprint – HPE ProLiant MicroServer Gen10 Plus data sheet" data-analytics-assettype="data sheet" data-analytics-assetid="a50005154enw">PCF: HPE ProLiant MicroServer Gen10 Plus</a> ...
  htmllink=`echo "$line" | sed -n -E 's/.*href\=\"([^\"]*html).*/\1/p'`
  fullname=`echo "$line" | sed -n -E 's/.*\>PCF: ([^\<]*)\<.*/\1/p'`

  if [ ! "$fullname" = "FAQ" ]; then
    echo $fullname
    echo $htmllink

    htmlfile=`echo "$htmllink" | sed -n -E 's/.*\/([^\/]*\.html).*/\1/p'`
    echo $htmlfile
    targethtml="$workdir/$htmlfile"
    if [ -f "$targethtml" ] ; then
      echo "$targethtml" OK
    else
      wget "$htmllink"
      mv $htmlfile $workdir
      sleep 2s
    fi

    # ... href="https://assets.ext.hpe.com/is/content/hpedam/documents/a50002000-2999/a50002755/a50002755enw.pdf"  ...
    pdflink=`grep ".pdf" "$targethtml" | sed -n -E 's/.*href\=\"([^\"]*\.pdf).*/\1/p'`

    "$script_path/"dl_and_append.sh "$pdflink" "$workdir" "$fullname" "Server" "$logfile"

    echo ""
  fi
  
done <<< "$content"