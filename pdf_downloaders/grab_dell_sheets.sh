#!/bin/bash

# This file is part of ecodiag-data, a set of scripts to assemble
# environnemental footprint data of ICT devices.
# 
# Copyright (C) 2021 Gael Guennebaud <gael.guennebaud@inria.fr>
# 
# This Source Code Form is subject to the terms of the Mozilla
# Public License v. 2.0. If a copy of the MPL was not distributed
# with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

# PROCEDURE:
# goto: https://corporate.delltechnologies.com/en-ie/social-impact/advancing-sustainability/sustainable-products-and-services/product-carbon-footprints.htm#tab0=0
# open all tabs to load all datas
# get actual html code by entering in the JS console:
#     new XMLSerializer().serializeToString(document.doctype) + document.getElementsByTagName('html')[0].outerHTML
# copy the single-line string into a .html file and pass it to this script !

bashver=`echo $BASH_VERSION | sed -n -E 's/([0-9]+)\..*/\1/p'`

if [ $bashver -lt 4 ]; then
  echo "Bash version: $bashver"
  echo "Error: need bash version 4 or higher"
  exit 1
fi

script_path=$(dirname "$0")

content=`sed -E 's/(\<\/[^\>]*\>)/\1\n/g' $1 |  sed -E 's/(\\\n|\\\t)//g' | sed -E 's/  +/ /g' | sed -E 's/&nbsp;/ /g' | egrep 'href.*pdf.*aria-label=|h2 class="tab\-heading"'`


# some sheets have been updated, but are missing in https://corporate.delltechnologies.com/en-ie/social-impact/advancing-sustainability/sustainable-products-and-services/product-carbon-footprints.htm
declare -A file2file=(
  ["http://i.dell.com/sites/csdocuments/Shared-Content_data-Sheets_Documents/en/P2418HZM-Monitor.pdf"]="https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-p2418hzm-monitor.pdf"
  ["http://i.dell.com/sites/csdocuments/Shared-Content_data-Sheets_Documents/en/P2419HC-Monitor.pdf"]="https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-p2419hc-monitor.pdf"
  ["http://i.dell.com/sites/csdocuments/Shared-Content_data-Sheets_Documents/en/P2418HZM-Monitor.pdf"]="https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-p2418hzm-monitor.pdf"
  ["http://i.dell.com/sites/csdocuments/Shared-Content_data-Sheets_Documents/en/P2419HC-Monitor.pdf"]="https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-p2419hc-monitor.pdf"
  ["https://corporate.delltechnologies.com//asset/en-ie/products/workstations/technical-support/precision-3560-pcf-datasheet.pdf"]="https://corporate.delltechnologies.com/content/dam/documents-and-videos/dv1/en/products/workstations/technical-support/precision-3560-pcf-datasheet.pdf")

# ["https://corporate.delltechnologies.com//asset/en-ie/products/thin-clients/technical-support/wyse-3040-thin-client-pcf-datasheet.pdf"]="https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-wyse-3040.pdf"
# ["https://corporate.delltechnologies.com//asset/en-ie/products/thin-clients/technical-support/wyse-5070-thin-client-pcf-datasheet.pdf"]="https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-wyse-5070.pdf"


baseurl="https://corporate.delltechnologies.com/"

workdir="cache/DELL"
logfile="cache/dell_files.csv"

echo \"filename\",\"category\",\"fullname\",\"url\" > $logfile

declare -A cat2dir=(  ["Laptops"]="cache/Laptop/DELL"
                      ["Desktops"]="cache/Desktop/DELL"
                      ["Monitors"]="cache/Monitor/DELL"
                      ["Servers"]="cache/Servers/DELL"
                      ["Wyse Thin Clients"]="cache/WyseThinClients/DELL"
                      ["Storage"]="cache/Storage/DELL"
                      ["Archive"]="cache/Archive/DELL" )

declare -A cat2cat=(  ["Laptops"]="Laptop"
                      ["Desktops"]="Desktop"
                      ["Monitors"]="Monitor"
                      ["Servers"]="Server"
                      ["Wyse Thin Clients"]="WyseThinClients"
                      ["Storage"]="Storage"
                      ["Archive"]="Archive" )

category="Archive"
while IFS= read -r line; do

  tmp=`echo "$line" | sed -n -E 's/.*tab\-heading\"\>(.*)\<.*/\1/p'`
  if [ -n "$tmp" ]; then
    echo "CATEGORY" $tmp
    category=$tmp
  else

    #... href="https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-wyse-7040.pdf" aria-label="Wyse 7040 Thin Clients" ...
    item=`echo "$line" | sed -n -E 's/.*href\=\"([^\"]*pdf).*aria\-label\=\"([^\"]*)\".*/\1  \2/p'`
    item=`echo "$item" | sed -E 's/&nbsp;/ /g'`

    if [ -n "$item" ]; then
      url=`echo $item | sed -n -E 's/([^ ]*) .*/\1/p'`
      fullname=`echo $item | sed -n -E 's/[^ ]+ (.*)/\1/p'`
      # echo $filename $fullname
      if ! echo $url | grep -q http; then
        if echo $url | grep -q '//'; then
          url="https:"$url
        else
          url=$baseurl$url
        fi
      fi

      url2=${file2file["$url"]}
      if [ -n "$url2" ]; then
        echo "Change url $url to $url2"
        url=$url2
      fi

      "$script_path/"dl_and_append.sh "$url" "${cat2dir["$category"]}" "$fullname" "${cat2cat["$category"]}" "$logfile"
      
  fi

  fi
  
done <<< "$content"

# add missing files:
"$script_path/"dl_and_append.sh "https://i.dell.com/sites/csdocuments/CorpComm_Docs/en/carbon-footprint-poweredge-r740.pdf" "${cat2dir["Servers"]}" "PowerEdge R740 (LCA)" "Server" "$logfile"

# check whether we have properly found ALL pdf:

content=`sed -E 's/(\<\/[^\>]*\>)/\1\n/g' $1 |  sed -E 's/(\\\n|\\\t)//g' | sed -E 's/  +/ /g' | egrep 'href.*pdf'`
while IFS= read -r line; do
  f=`echo $line | sed -n -E 's/.*\/([^\/]*\.pdf).*/\1/p'`
  if ! grep -q "$f" "$logfile" ; then
    echo $f MISSING
  fi
done <<< "$content"
